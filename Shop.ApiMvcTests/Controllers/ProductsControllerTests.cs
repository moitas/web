using Moq;
using NUnit.Framework;
using Shop.ApiMvc.Controllers;
using Shop.CORE;
using Shop.CORE.Contracts;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;

namespace Shop.ApiMvcTests.Controllers
{
    [TestFixture]
    public class ProductsControllerTests
    {
        private MockRepository mockRepository;

        private Mock<IProductManager> mockProductManager;

        [SetUp]
        public void SetUp()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);

            this.mockProductManager = this.mockRepository.Create<IProductManager>();
        }

        [TearDown]
        public void TearDown()
        {
            this.mockRepository.VerifyAll();
        }

        [Test]
        public void TestMethod1()
        {
            // Arrange


            // Act
            ProductsController productsController = this.CreateProductsController();


            // Assert

        }

        private ProductsController CreateProductsController()
        {
            return new ProductsController(
                this.mockProductManager.Object);
        }


        [Test]
        public void PutReturnsContentResult()
        {
            // Arrange
            var mockRepository = new Mock<IProductManager>();
            var controller = new ProductsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.PutProduct(2,
                new Product { ProductId = 10, Description = "Product1", Price = 5, Stock = 5 });
            var contentResult = actionResult as NegotiatedContentResult<Product>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.AreEqual(HttpStatusCode.Accepted, contentResult.StatusCode);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(10, contentResult.Content.ProductId);
        }



    }
}
