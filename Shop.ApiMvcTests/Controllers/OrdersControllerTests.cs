using Moq;
using NUnit.Framework;
using Shop.ApiMvc.Controllers;
using Shop.CORE.Contracts;

namespace Shop.ApiMvcTests.Controllers
{
    [TestFixture]
    public class OrdersControllerTests
    {
        private MockRepository mockRepository;

        private Mock<IOrderManager> mockOrderManager;

        [SetUp]
        public void SetUp()
        {
            this.mockRepository = new MockRepository(MockBehavior.Strict);

            this.mockOrderManager = this.mockRepository.Create<IOrderManager>();
        }

        [TearDown]
        public void TearDown()
        {
            this.mockRepository.VerifyAll();
        }

        [Test]
        public void TestMethod1()
        {
            // Arrange


            // Act
            OrdersController ordersController = this.CreateOrdersController();


            // Assert

        }

        private OrdersController CreateOrdersController()
        {
            return new OrdersController(
                this.mockOrderManager.Object);
        }
    }
}
