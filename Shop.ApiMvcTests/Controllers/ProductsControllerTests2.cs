using Moq;
//using NUnit.Framework;
using Shop.ApiMvc.Controllers;
using Shop.CORE;
using Shop.CORE.Contracts;
using System.Net;
using System.Web.Http;
using System.Web.Http.Results;
using Microsoft.VisualStudio.TestTools.UnitTesting;


using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;




namespace Shop.ApiMvcTests.Controllers
{
    [TestClass()]
    public class ProductsController2Tests
    {
        [TestMethod]
        public void WebApiGetReturnsProductWithSameIdTest()
        {
            // Arrange
            var mockRepository = new Mock<IProductManager>();
            mockRepository.Setup(x => x.GetById(42))
                .Returns(new Product { ProductId = 42 });

            var controller = new ProductsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetProduct(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Product>;

            // Assert
            Assert.IsNotNull(contentResult);
            Assert.IsNotNull(contentResult.Content);
            Assert.AreEqual(42, contentResult.Content.ProductId);
        }

        [TestMethod]
        public void WebApiGetProducts_ShouldReturnAllProductsTest()
        {

            var ProductList = new List<Product>
            {
                new Product() { ProductId = 1, Description="uno", Price=6, Stock=5 },
                new Product() { ProductId = 2,  Description="dos", Price=4, Stock=5},
              new Product() { ProductId = 3,  Description="tres", Price=4, Stock=0}
            }.AsQueryable();

            var mockRepository = new Mock<IProductManager>();

            mockRepository
                .Setup(x => x.GetAll())
                .Returns(ProductList);

            var controller = new ProductsController(mockRepository.Object);

            var result = controller.GetProducts();

            Assert.IsNotNull(result);
            Assert.AreEqual(3, result.Count());
        }


        [TestMethod]
        public void WebApiGetProduct_ShouldNotFindProduct()
        {
            var ProductList = new List<Product>
            {
                new Product() { ProductId = 1, Description="uno", Price=6, Stock=5 },
                new Product() { ProductId = 2,  Description="dos", Price=4, Stock=5},
              new Product() { ProductId = 3,  Description="tres", Price=4, Stock=0}
            }.AsQueryable();

            var mockRepository = new Mock<IProductManager>();

            mockRepository
                .Setup(x => x.GetAll())
                .Returns(ProductList);

            var controller = new ProductsController(mockRepository.Object);

            // Act
            IHttpActionResult actionResult = controller.GetProduct(42);
            var contentResult = actionResult as OkNegotiatedContentResult<Product>;

            // Assert
            Assert.IsNull(contentResult);
        }
    }
}
