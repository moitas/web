﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace Shop.MVC.Adapter
{
    public static class HtmlHelpers
    {
        public static HtmlHelperAdapter<TModel> Adapt<TModel>(this HtmlHelper<TModel> helper)
        {
            return new HtmlHelperAdapter<TModel>(helper);
        }

        public class HtmlHelperAdapter<TModel>
        {
            private readonly HtmlHelper<TModel> _helper;

            public HtmlHelperAdapter(HtmlHelper<TModel> helper)
            {
                _helper = helper;
            }

            public HtmlHelper<TNewModel> For<TNewModel>()
            {
                return new HtmlHelper<TNewModel>(_helper.ViewContext, new ViewPage(), _helper.RouteCollection);
            }
        }
    }
}