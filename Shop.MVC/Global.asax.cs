﻿using Shop.CORE;
using Shop.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Optimization;
using System.Web.Routing;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Shop.MVC.Binders;

namespace Shop.MVC
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            AreaRegistration.RegisterAllAreas();
            FilterConfig.RegisterGlobalFilters(GlobalFilters.Filters);
            RouteConfig.RegisterRoutes(RouteTable.Routes);
            BundleConfig.RegisterBundles(BundleTable.Bundles);
            ModelBinders.Binders.Add(typeof(decimal), new DecimalModelBinder());
            ModelBinders.Binders.Add(typeof(decimal?), new DecimalModelBinder());

            ////Configuramos seguridad
            ApplicationDbContext context = new ApplicationDbContext();
            RoleManager<IdentityRole> roleManager =
                new RoleManager<IdentityRole>(
                    new RoleStore<IdentityRole>(context));
            UserManager<ApplicationUser> userManager =
                new UserManager<ApplicationUser>(
                    new UserStore<ApplicationUser>(context));

            if (!roleManager.RoleExists("Admin"))
                roleManager.Create(new IdentityRole("Admin"));

            if (!roleManager.RoleExists("Client"))
                roleManager.Create(new IdentityRole("Client"));


            //ApplicationUser user = userManager.FindByName("admin@admin.com");
            //if (user == null)
            //{
            //    user = new ApplicationUser();
            //    user.UserName = "admin@admin.com";
            //    user.Email = "admin@admin.com";
            //    user.FirstName = "Pepe";
            //    user.LastName = "Perez";
            //    user.Address = "a";
            //    user.City = "e";
            //    user.PostalCode = "r";
            //    IdentityResult result = userManager.Create(user, "@User123");
            //    if (result.Succeeded)
            //    {
            //        userManager.AddToRole(user.Id, "Admin");
            //    }
            //    else
            //    {
            //        throw new Exception("Usuario no creado");
            //    }
            //}
            //else
            //{
            //    //El usuario está creado, ¿Pero ya esta en el rol admin?
            //    if (!userManager.IsInRole(user.Id, "Admin"))
            //    {
            //        userManager.AddToRole(user.Id, "Admin");
            //    }
            //}


            //ApplicationUser user2 = userManager.FindByName("seasincidencias2016 @gmail.com");

            //if (user2 == null)
            //{
            //    user2 = new ApplicationUser();
            //    user2.UserName = "seasincidencias2016@gmail.com";
            //    user2.Email = "seasincidencias2016@gmail.com";
            //    user2.FirstName = "Pepe";
            //    user2.LastName = "Perez";
            //    IdentityResult result = userManager.Create(user2, "@User123");
            //    if (result.Succeeded)
            //    {
            //        userManager.AddToRole(user2.Id, "Admin");
            //    }
            //    else
            //    {
            //        throw new Exception("Usuario no creado");
            //    }
            //}
            //else
            //{
            //    //El usuario está creado, ¿Pero ya esta en el rol admin?
            //    if (!userManager.IsInRole(user2.Id, "Admin"))
            //    {
            //        userManager.AddToRole(user2.Id, "Admin");
            //    }
            //}


        }

    }
}