﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Owin;
using Owin;
using Shop.CORE;
using Shop.DAL;

[assembly: OwinStartupAttribute(typeof(Shop.MVC.Startup))]
namespace Shop.MVC
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }



    }
}
