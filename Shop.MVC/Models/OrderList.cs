﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace Shop.MVC.Models
{
    public class OrderList:CORE.Order
    {

       /// <summary>
       /// Cuenta de correo
       /// </summary>
        public virtual string Mail { get; set; }

        [Display(Name = "Total(€)")]// abbreviation shown in Html.DisplayNameFor
        public decimal Total { get; set; }


    }
}