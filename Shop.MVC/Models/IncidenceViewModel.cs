﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.MVC.Models
{
    public class IncidenceViewModel
    {
        /// <summary>
        /// Identificador de la incidencia
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del equipo
        /// </summary>      
        [Required]
        [Display(Name ="Nombre del equipo")]
        public string Equipment { get; set; }

        /// <summary>
        /// Fecha de creación de la incidencia
        /// </summary>
        public DateTime CreatedDate { get; set; }


        /// <summary>
        /// Tipo de incidencia
        /// </summary>
        //public IncidenceType IncidenceType { get; set; }
    }
}
