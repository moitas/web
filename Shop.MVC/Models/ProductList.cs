﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.MVC.Models
{
    public class ProductList:CORE.Product
    {
        /// <summary>
        /// Clases de imagenes de conversion de formatos
        /// </summary>
        public string Image1Base64
        {
            get
            {
                if (ProductImage1 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage1);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }


        public string Image2Base64
        {
            get
            {
                if (ProductImage2 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage2);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }

        public string Image3Base64
        {
            get
            {
                if (ProductImage3 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage3);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }


    }
}