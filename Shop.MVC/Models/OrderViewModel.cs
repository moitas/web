﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.MVC.Models
{
    public class OrderViewModel : CORE.Order
    {


        public virtual List<OrderLineViewModel> OrderLinesVM { get; set; }


        /// <summary>
        /// Direccion de envio abreviada del envio
        /// </summary>
        [Display(Name = "Descripcion")]
        public string AddressVM
        {
            get
            {
                return "Fue enviado a esta dirección: " + Street + "-" + City+ "-" + ZipCode;
            }
        }


        /// <summary>
        /// Dirección de envio abreviada del pago
        /// </summary>
        [Display(Name = "Dirección de envio")]
        public string AddressVMPago
        {
            get
            {
                return "Será enviado a: " + Street + "-" + City+ "-" + ZipCode;
            }
        }


        [Display(Name = "Total a pagar(€)")]
        public decimal TotalPago
        {
            get
            {
                return TotalPay;
            }
        }

        /// <summary>
        /// Datos de la tarjeta de  pago Cvv
        /// </summary>
        [RegularExpression(@"^\d{3}$", ErrorMessage = "CVV no valido"), Required]
        public string Cvv { get; set; }

        /// <summary>
        /// Mes de caducidad de la tarjeta
        /// </summary>
        [RegularExpression(@"^(0[1-9]|1[0-2])$", ErrorMessage = "Mes no valido MM"), Required]
        public string ExpirationMonth { get; set; }

        /// <summary>
        /// Año de expiración de la tarjeta
        /// </summary>
        [RegularExpression(@"^20[0-9]{2}$", ErrorMessage = "Año no valido YY"), Required]
        public string ExpirationYear { get; set; }
    }


}