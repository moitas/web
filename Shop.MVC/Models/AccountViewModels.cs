﻿using Microsoft.AspNet.Identity.EntityFramework;
using Shop.CORE;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Shop.MVC.Models
{
    public class ExternalLoginConfirmationViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class ExternalLoginListViewModel
    {
        public string ReturnUrl { get; set; }
    }

    public class SendCodeViewModel
    {
        public string SelectedProvider { get; set; }
        public ICollection<System.Web.Mvc.SelectListItem> Providers { get; set; }
        public string ReturnUrl { get; set; }
        public bool RememberMe { get; set; }
    }

    public class VerifyCodeViewModel
    {
        [Required]
        public string Provider { get; set; }

        [Required]
        [Display(Name = "Code")]
        public string Code { get; set; }
        public string ReturnUrl { get; set; }

        [Display(Name = "Remember this browser?")]
        public bool RememberBrowser { get; set; }

        public bool RememberMe { get; set; }
    }

    public class ForgotViewModel
    {
        [Required]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }

    public class LoginViewModel
    {
        [Required]
        [Display(Name = "Email")]
        [EmailAddress]
        public string Email { get; set; }

        [Required]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [Display(Name = "Recuerdame")]
        public bool RememberMe { get; set; }
    }

    public class RegisterViewModel
    {

        //PUESTA POR MI PARA GESTIONAR LOS ROLES

        //[Required]
        //[Display(Name = "UserRole")]
        //public string UserRole { get; set; }

        //[Required]
        //[Display(Name = "UserName")]
        //public string UserName { get; set; }

        //https://code.msdn.microsoft.com/ASPNET-MVC-5-Security-And-44cbdb97


        //[Required(ErrorMessage = "First name is required.")]
        //[StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        //public string FirstName { get; set; }

        //[Required(ErrorMessage = "Last name is required.")]
        //[StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        //public string LastName { get; set; }


        /// <summary>
        /// Nombre del usuario registrado
        /// </summary>
        /// 
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Nombre requerido")]
        [StringLength(30, ErrorMessage = "Nombre tiene más de 30 caracteres")]
        public string FirstName { get; set; }


        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Apellido requerido")]
        [StringLength(30, ErrorMessage = "Apellido tiene más de 30 caracteres")]
        public string LastName { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        /// 
        [Required(ErrorMessage = "Codigo postal requerido")]
        [Display(Name = "Cod-Postal")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Ciudad
        /// </summary>
        /// 
        [Display(Name = "Ciudad")]
        [Required(ErrorMessage = "Ciudad requerida")]
        [StringLength(30, ErrorMessage = "Ciudad tiene más de 30 caracteres")]
        public string City { get; set; }


        /// <summary>
        /// Dirección del usuario
        /// </summary>
        /// 
        [Required(ErrorMessage = "Direccion requerida")]
        [Display(Name = "Dirección")]
        [StringLength(40, ErrorMessage = "Dirección tiene más de 40 caracteres")]
        public string Address { get; set; }

        //[Display(Name = "Teléfono")]
        //[StringLength(9, ErrorMessage = " tiene más de 9 caracteres")]
        //public string PhoneNumber { get; set; }




        ///// <summary>
        ///// Codigo postal
        ///// </summary>
        ///// 
        ////[Required]
        //[Display(Name = "Postal Code")]
        //[DataType(DataType.PostalCode)]
        //[StringLength(30, ErrorMessage = "Name may not be longer than 5 characters")]
        //public string PostalCode { get; set; }

        ///// <summary>
        ///// Ciudad
        ///// </summary>
        ///// 
        //[Display(Name = "Ciudad")]
        //[StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        //public string City { get; set; }

        //[Display(Name = "Dirrección")]
        //[StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        //public string Address { get; set; }


        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }




    }





    public class ResetPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        public string Code { get; set; }
    }

    public class ForgotPasswordViewModel
    {
        [Required]
        [EmailAddress]
        [Display(Name = "Email")]
        public string Email { get; set; }
    }











    public class EditViewModel
    {
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Se requiere el nombre.")]
        [StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        public string FirstName { get; set; }

        [Display(Name = "Apellidos")]
        [Required(ErrorMessage = "Se requiere apellidos")]
        [StringLength(30, ErrorMessage = "Apellidos no debe tener más de 30 caracteres")]
        public string LastName { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        /// 
        [Required(ErrorMessage = "Se requiere el Cod-Postal.")]
        [Display(Name = "Cod-Postal")]
        [DataType(DataType.PostalCode)]
        [StringLength(5, ErrorMessage = "Cod-Postal no debe tener más de 5 caracteres")]
        public string PostalCode { get; set; }

        /// <summary>
        /// Ciudad
        /// </summary>
        /// 
        [Required(ErrorMessage = "Se requiere la ciudad.")]
        [Display(Name = "Ciudad")]
        [StringLength(30, ErrorMessage = "Ciudad no debe tener más de 30 caracteres")]
        public string City { get; set; }

        /// <summary>
        /// Direccion del usuario
        /// </summary>
        /// 
        [Required(ErrorMessage = "Se requiere la dirección.")]
        [Display(Name = "Dirección")]
        [StringLength(30, ErrorMessage = "Dirección no debe tener más de 30 caracteres")]
        public string Address { get; set; }


        [Required(ErrorMessage = "Se requiere telf")]
        [Display(Name = "Teléfono")]
        [Phone(ErrorMessage = "Formato no válido")]
        public string PhoneNumber { get; set; }



    }




    public class UserViewModel
    {
        [Display(Name = "NetworkID")]
        [Required(ErrorMessage = "NetworkID is required!")]
        [RegularExpression(@"([A-Za-z0-9.-])+", ErrorMessage = "Invalid!")]
        public string Id { get; set; }

        /// <summary>
        /// Nombre del cliente
        /// </summary>
        [Display(Name = "Nombre")]
        public string FirstName { get; set; }

        /// <summary>
        /// Apellido del usuario
        /// </summary>
        /// 
        [Display(Name = "Apellido")]
        public string LastName { get; set; }

        /// <summary>
        /// Apellido del usuario
        /// </summary>
        /// 
        [Display(Name = "Telefono")]
        public string PhoneNumber { get; set; }


        [Display(Name = "Email")]
        public string Email { get; set; }




        public bool IsAdmin { get; set; }

        public string Role
        {
            get
            {
                if (IsAdmin)
                {
                    return "Admin";
                }
                return "Employee";
            }
        }

  }

}











