﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shop.MVC.Models
{
    public class ProductCreateViewModel
    {

        /// <summary>
        /// Identificador del producto
        /// </summary>
        /// 
        [Display(Name = "COD")]
        public int ProductId { get; set; }

        
        /// <summary>
        /// Descripcion del producto
        /// </summary>
        /// 
        [MaxLength(100), MinLength(3)]
        [Required(ErrorMessage = "Nada de anónimos. ¡Aquí todo tiene un nombre!")]
        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        /// <summary>
        /// Descripcion extendida del producto
        /// </summary>
        /// 
        [Required(ErrorMessage = "Falta el detalle")]
        [MaxLength(100)]
        [Display(Name = "Detalle")]        /// 
        public string DescriptionLarge { get; set; }


        /// <summary>
        /// Cantidad de producto en unidades
        /// </summary>
        /// 
        [Required(ErrorMessage = "Verifica el stock entre 0-999")]
        [Display(Name = "Stock")]
        [Range(0, 999)]
        public int Stock { get; set; }


        /// <summary>
        /// Precio del producto actual
        /// </summary>
        /// 
        [Display(Name = "Precio(€)")]
        [Required(ErrorMessage = "Verifica el precio")]
        [Range(1, 999, ErrorMessage = "Entre de 1 9999")]
        public decimal Price { get; set; }

        /// <summary>
        /// Imagen  número 1
        /// </summary>
        /// 
        [Display(Name = "Imagen principal")]
        public byte[] ProductImage1 { get; set; }

        /// <summary>
        /// Imagen número 2
        /// </summary>
        /// 
        [Display(Name = "Imagen 2º")]
        public byte[] ProductImage2 { get; set; }

        /// <summary>
        /// Imagen numero 3
        /// </summary>
        /// 
        [Display(Name = "Imagen 3º")]
        public byte[] ProductImage3 { get; set; }



        //CAMPOS NO CORE

        //[Display(Name = "Unidades")]
        //[Range(1, 999, ErrorMessage = "Entre de 1 999")]
        //public int Quantity { get; set; }

        //[RegularExpression("^[0-9]*$", ErrorMessage = "* Solo se permiten números.")]
        //[Range(01, 99, ErrorMessage = "El Campo {0} debe ser un numero entre 01 y {2}")]
           //[RegularExpression("^\\d+$", ErrorMessage = "La cantidad debe contener sólo números.")]
        //[Required(ErrorMessage = "El {0} es obligatorio")]
        //[DefaultValue(33)]
        //[Required(ErrorMessage = "Debe de ingresar una cantidad")]

        /// <summary>
        /// Imagen 1 en Base64
        /// </summary>
        public string Image1Base64
        {
            get
            {
                if (ProductImage1 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage1);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }


        /// <summary>
        /// Imagen 2 en Base 64
        /// </summary>
        public string Image2Base64
        {
            get
            {
                if (ProductImage2 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage2);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }


        /// <summary>
        /// Imagen 3 en base 64
        /// </summary>
        public string Image3Base64
        {
            get
            {
                if (ProductImage3 == null)
                {
                    return string.Empty;
                }
                else
                {
                    string base64 = Convert.ToBase64String(this.ProductImage3);
                    return String.Format("data:image/gif;base64,{0}", base64);
                }
            }
        }




    }
}