﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Shop.MVC.Models
{
    public class CartViewModel
    {

        /// <summary>
        /// Identificador de la compra
        /// </summary>
        /// 
        [Key]
        public int Id { get; set; }





        /// <summary>
        /// Identificador del usuario que ha realizado la compra
        /// </summary>
        public string User_Id { get; set; }


        /// <summary>
        /// Colección de productos
        /// </summary>
        public virtual List<CartItemList> CartItems { get; set; }



    }
}
