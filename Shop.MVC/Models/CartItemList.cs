﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;


namespace Shop.MVC.Models
{
    public class CartItemList
    {
        /// <summary>
        /// Identificador del linea
        /// </summary>
        /// 
        [Display(Name = "Linea")]
        public int ItemId { get; set; }

        ///// <summary>
        ///// Id del prodcuto
        ///// </summary>
        [Display(Name = "Cod")]
        public int ProductId { get; set; }

        /// <summary>
        /// Descripción del prodcuto
        /// </summary>
        /// 
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        /// 
        [Display(Name = "Precio")]
        public decimal Price { get; set; }

        /// <summary>
        /// Cantidad del prodcuto
        /// </summary>
        /// 
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }

        /// <summary>
        /// Identificador del producto
        /// </summary>
        /// 
        //public int CartId { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public decimal Subtotal { get; set; }

        /// <summary>
        /// Identificador del usuario
        /// </summary>
        public string User_Id { get; set; }


        public virtual ProductViewModel Product { get; set; }

    }
}
