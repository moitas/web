﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Shop.MVC.Models
{
    public class ItemList
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Nombre del prodcuto
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Foto del producto
        /// </summary>
       //NO LO SE
        /// <summary>
        /// Descripción del prodcuto
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto
        /// </summary>
        public float Price { get; set; }

        /// <summary>
        /// Cantidad del prodcuto
        /// </summary>
        public int Quantity { get; set; }
    }
}