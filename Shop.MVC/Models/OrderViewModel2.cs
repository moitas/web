﻿using Shop.CORE;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Shop.MVC.Models
{
    public class OrderViewModel2
    {
        /// <summary>
        /// Identificador de la compra
        /// </summary>
        /// 
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Fecha en la que se ha realizado la compra
        /// </summary>
        /// 
        [Required]
        [Display(Name = "Fecha de creación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha de envio
        /// </summary>
        /// 
        [Display(Name = "Fecha de envio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime? DateSend { get; set; }

        /// <summary>
        /// Fecha de recepcion
        /// </summary>
        /// 
        [Display(Name = "Fecha de recepción")]// abbreviation shown in Html.DisplayNameFor
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime? DateReception { get; set; }

        /// <summary>
        /// Estado del pedido (Enumerado)
        /// </summary>
        public OrderStatus Status { get; set; }

    }
}