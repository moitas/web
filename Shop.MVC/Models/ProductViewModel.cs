﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;

namespace Shop.MVC.Models
{
    public class ProductViewModel: ProductCreateViewModel
    {
        /// <summary>
        /// Cantidad requerida del producto
        /// </summary>
        [Display(Name = "Unidades")]
        [Range(1, 999, ErrorMessage = "Entre de 1 999")]
        public int Quantity { get; set; }

    }
}