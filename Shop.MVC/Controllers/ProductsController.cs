﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Shop.CORE;
using Shop.DAL;
using Shop.CORE.Contracts;
using Shop.MVC.Models;
using System.IO;
using Shop.IFR.Tools;
using Microsoft.AspNet.Identity;
using Shop.IFR.Logging;

namespace Shop.MVC.Controllers
{
    [ExceptionHandler]
    public class ProductsController : Controller
    {
        IProductManager productManager = null;
        SessionController session = null;

        public ProductsController(IProductManager productManager)
        {
            this.productManager = productManager;
            this.session = new SessionController();
        }


        //*******************************************METODO DE LISTADOS DE PRODUCTO -ANONIMO*************************************************************
        /// <summary>
        /// METODO GET DE LISTADO DE PRODUCTOS ACCESO ANONIMO/ PERSONALIZADO EN VISTA
        /// </summary>
        /// <returns></returns>
        [AllowAnonymous]
        // GET: Products
        public ActionResult Index()
        {
                var result = productManager.GetAll().Select(e => new ProductList
                {

                    ProductId = e.ProductId,
                    Description = e.Description,
                    DescriptionLarge = e.DescriptionLarge,
                    Price = e.Price,
                    Stock = e.Stock,
                    ProductImage1 = e.ProductImage1,
                    ProductImage2 = e.ProductImage2,
                    ProductImage3 = e.ProductImage3,
                });

                return View(result);
              
        }


        //*******************************************METODOS DE DETALLE PRODUCTO -SOLO CLIENTE*************************************************************
        /// <summary>
        /// mETODO GET DE DETALLE DE PRODUCTO SOLO ROLE CLIENT
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: Products/Details/5
        public ActionResult Details(int? id)
        {
            try
            {
                if (id == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                var e = productManager.GetById(id.Value);
                if (e == null)
                {
                    return HttpNotFound();
                }

                ProductViewModel p = new ProductViewModel
                {
                    Description = e.Description,
                    DescriptionLarge = e.DescriptionLarge,
                    ProductId = e.ProductId,
                    Price = e.Price,
                    ProductImage1 = e.ProductImage1,
                    ProductImage2 = e.ProductImage2,
                    ProductImage3 = e.ProductImage3,
                    Quantity = 1,
                };

                return View(p);
            }

            catch (Exception ex)
            {
                ModelState.AddModelError("keyName", ex.Message);
                return RedirectToAction("Error", "Shared");

            }

        }

        /// <summary>
        /// METODO POST DE DETALLE DE PRODUCTO SOLO ROLE CLIENT
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Details([Bind(Include = "ProductId, Quantity")] ProductViewModel e)
        public ActionResult Details(ProductViewModel e)
        {

            var errors = ModelState.Where(x => x.Value.Errors.Count > 0)
          .Select(x => new { x.Key, x.Value.Errors })
          .ToArray();

            Product p = productManager.GetById(e.ProductId);
            
            if (p.Stock<e.Quantity)     
            {
                ModelState.AddModelError("quantity", "Stock insuficiente");
                return View(e);
            }

            if (!ModelState.IsValid)
            {
                ModelState.AddModelError("keyName", "Falllo en modelo de producto");
                return RedirectToAction("Error", "Shared");
            }

            try
            {

                if (ModelState.IsValid)
                {

                    CartItem ci = new CartItem
                    {
                        Description = p.Description,
                        Price = p.Price,
                        //Product = p,
                        Quantity = e.Quantity,
                        ProductId = e.ProductId,
                    };

                    this.session.addCartItem(ci);

                    return RedirectToAction("Index", "Carts");
                }
                else
                {
                    return RedirectToAction("Details", "Products", new { id = e.ProductId });
                }

            }

            catch (Exception ex)
            {
                ModelState.AddModelError("keyName", ex.Message);
                return RedirectToAction("Error", "Shared");

            }
        }


        //*******************************************METODOS DE CREACION PRODUCTO -SOLO ADMIN*************************************************************
        /// <summary>
        /// METODO QUE RETORNA LA VISTA DE CREACION DEL PRODUCTO SOLO ADMIN
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // GET: Products/Create
        public ActionResult Create()
        {
            return View();
        }

        /// <summary>
        /// mETODO POST DE CREACCION DEL PRODUCTO
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // POST: Products/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ProductId,Description,DescriptionLarge,Price,Stock,ProductImage1,ProductImage2,ProductImage3,Image1Base64")] ProductCreateViewModel e)
        {
            try
            {
                 if (!ModelState.IsValid)
                {
                    ModelState.AddModelError("keyName", "Error en el modelo de creación de producto");
                    return RedirectToAction("Error", "Shared");
                }

                    HttpPostedFileBase file1 = Request.Files["ImageData1"];
                    HttpPostedFileBase file2 = Request.Files["ImageData2"];
                    HttpPostedFileBase file3 = Request.Files["ImageData3"];

                    /// p es el objeto tipo core
                    Product p = new Product
                    {
                        Description = e.Description,
                        DescriptionLarge = e.DescriptionLarge,
                        ProductId = e.ProductId,
                        Price = e.Price,
                        Stock = e.Stock,
                        ProductImage1 = ServiceImage.ConvertToBytes(file1),
                        ProductImage2 = ServiceImage.ConvertToBytes(file2),
                        ProductImage3 = ServiceImage.ConvertToBytes(file3),
                    };

                    productManager.Add(p);
                    productManager.Context.SaveChanges();

                return RedirectToAction("Index", "Products");

            }
            ///Esto retorna la vista para que no se vacien los campos
            //return View(e);

            catch (Exception ex)
            {
                ModelState.AddModelError("keyName", ex.Message);
                return RedirectToAction("Error", "Shared");
            }

        }


        //*******************************************METODOS DE EDICION PRODUCTO -SOLO ADMIN*************************************************************
        /// <summary>
        /// mERODO GET DE EDICION DEL PRODUCTO
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // GET: Products/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var e = productManager.GetById(id.Value);

            if (e != null)
            {

                ProductCreateViewModel p = new ProductCreateViewModel
                {
                    Description = e.Description,
                    DescriptionLarge = e.DescriptionLarge,
                    ProductId = e.ProductId,
                    Price = e.Price,
                    Stock=e.Stock,
                    ProductImage1 = e.ProductImage1,
                    ProductImage2 = e.ProductImage2,
                    ProductImage3 = e.ProductImage3,

                };

                return View(p);
            }

            else
                return RedirectToAction("Index");
        }

        /// <summary>
        /// METODO POST DE EDICION DEL PRODUCTO
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // POST: Products/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
       // public ActionResult Edit([Bind(Include = "ProductId,Description,DescriptionLarge,Price,Stock,ProductImage1,ProductImage2,ProductImage3")] ProductViewModel e)
        public ActionResult Edit(ProductCreateViewModel e)
        {

            if (ModelState.IsValid)
            {
                
                HttpPostedFileBase file1 = Request.Files["ImageData1"];
                HttpPostedFileBase file2 = Request.Files["ImageData2"];
                HttpPostedFileBase file3 = Request.Files["ImageData3"];

                /// p es el objeto tipo core
                byte[] image1 = ServiceImage.ConvertToBytes(file1);
                byte[] image2 = ServiceImage.ConvertToBytes(file2);
                byte[] image3 = ServiceImage.ConvertToBytes(file3);

                Product p = productManager.GetById(e.ProductId);

                p.Description = e.Description;
                p.DescriptionLarge = e.DescriptionLarge;
                //p.ProductId = e.ProductId;
                p.Price = e.Price;
                p.Stock = e.Stock;
                if (image1 != null && image1.Length > 0)
                {
                    p.ProductImage1 = image1;
                }
                if (image2 != null && image2.Length > 0)
                {
                    p.ProductImage2 = image2;
                }
                if (image3 != null && image3.Length > 0)
                {
                    p.ProductImage3 = image3;
                }
                productManager.Context.SaveChanges();

                return RedirectToAction("Index");
            }

            return RedirectToAction("Index", "Home");
            ///Esto retorna la vista para que no se vacien los campos
            //return View(e);
        }

    }
}

