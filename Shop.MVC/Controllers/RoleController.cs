﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Shop.MVC.Models;
using Shop.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using Shop.IFR.Tools;
using Shop.IFR.MailService;
using Shop.IFR.IoC;
using System.Net.Mail;
using System.Data.Entity;
using Microsoft.Owin;
using Shop.DAL;
using Shop.CORE.Contracts;

namespace Shop.MVC.Models
{
    public class RoleController : Controller
    {

        private ApplicationUserManager _userManager;
        private readonly IApplicationDbContext dbContext;
        private ApplicationRoleManager _roleManager;

        public RoleController()
        {
        }

        public RoleController(ApplicationUserManager userManager)
        {
            this.dbContext = dbContext;
            UserManager = userManager;


        }

        public class ApplicationRoleManager : RoleManager<IdentityRole>
        {
            public ApplicationRoleManager(IRoleStore<IdentityRole, string> roleStore)
            : base(roleStore)
            { }

            public static ApplicationRoleManager Create(
                IdentityFactoryOptions<ApplicationRoleManager> options,
                IOwinContext context)
            {
                var manager = new ApplicationRoleManager(
                    new RoleStore<IdentityRole>(context.Get<ApplicationDbContext>()));
                return manager;
            }
        }



        SessionController session = null;

        public ProductsController(IProductManager productManager)
        {
            this.dbContext = dbContext;
            this.productManager = productManager;
            this.session = new SessionController();
        }
        

        {

            get

            {

                return _roleManager ?? HttpContext.GetOwinContext().GetUserManager();

            }

            private set

            {

                _roleManager = value;

            }

        }


        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }

        public class ApplicationRole : IdentityRole
        {
            public ApplicationRole() : base() { }
            public ApplicationRole(string name, string description) : base(name)
            {
                this.Description = description;
            }
            public virtual string Description { get; set; }
        }


        /// <summary>
        /// Get All Roles
        /// </summary>
        /// <returns></returns>
        public ActionResult Index()
        {
            var currentRoles = new List<IdentityUserRole>();
 
            return View(currentRoles);
        }

        /// <summary>
        /// Create  a New role
        /// </summary>
        /// <returns></returns>
        public ActionResult Create()
        {
            // Swap ApplicationRole for IdentityRole:
            var idResult = _roleManager.Create(new ApplicationRole("name", "description"));
            return idResult.Succeeded;

        }

        /// <summary>
        /// Create a New Role
        /// </summary>
        /// <param name="Role"></param>
        /// <returns></returns>
        [HttpPost]
        public ActionResult Create(IdentityRole Role)
        {
            IdentityDbContext conte
            context.Roles.Add(Role);
            context.SaveChanges();
            return RedirectToAction("Index");
        }



    }
}