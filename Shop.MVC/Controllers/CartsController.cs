﻿using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Shop.CORE;
using Shop.DAL;
using Shop.CORE.Contracts;
using Shop.MVC.Models;
using System.IO;
using Shop.IFR.Tools;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using Microsoft.AspNet.Identity.Owin;
using System.Configuration;
using Shop.IFR.Logging;

namespace Shop.MVC.Controllers
{
    [ExceptionHandler]
    public class CartsController : Controller
    {
        //private ApplicationDbContext db = new ApplicationDbContext();
        IProductManager productManager = null;
        IOrderManager orderManager = null;
        SessionController session = null;
        
        public CartsController(IProductManager productManager, IOrderManager orderManager)
        {
            this.productManager = productManager;
            this.orderManager = orderManager;
            this.session = new SessionController();
        }

        //*******************************************METODO DE LISTADOS DE ITEMS EN CARRO *************************************************************
        /// <summary>
        /// METODO QUE RETORNA LAS LINEAS DEL CARRO DE LA SESION
        /// </summary>
        /// <returns></returns>
        // GET: Carts
        public ActionResult Index()
        {
            try
            {
                if (this.session.getCartItems() != null)
                {
                    var items = this.session.getCartItems()
                        .Select(e => new CartItemList
                        {
                            ItemId = e.Id,
                            ProductId = e.ProductId,
                            User_Id = e.User_Id,
                            //CartId = e.Cart_Id,
                            Description = e.Description,
                            Price = e.Price,
                            Quantity = e.Quantity,
                            Subtotal = e.Quantity * e.Price,
                        });

                    if (items == null)
                    {
                        //return RedirectToAction("Contact", "Home");
                        return HttpNotFound();
                    }
                    else
                    {
                        decimal total = items.Sum(s => s.Subtotal);
                        ViewBag.Total = total;

                        return View(items);
                    }
                } else
                {
                    return View();
                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
        }



        //*******************************************METODOS BOORRA ITEMS DEL CARRO ************************************************

        /// <summary>
        /// METODO GET PARA BORRA LINEAS DEL CARRO DE LA SESION
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>

        // GET: Carts/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            //Eliminacion de lineas
            this.session.removeCartItemByProductId(id.Value);

            return RedirectToAction("Index");

        }
    }
}





//var stripePublishKey = ConfigurationManager.AppSettings["stripePublishableKey"];
//ViewBag.StripePublishKey = stripePublishKey;               
//https://msdn.microsoft.com/es-es/library/6sh2ey19(v=vs.110).aspx
//https://msdn.microsoft.com/en-us/library/t16fe6f4(v=vs.120).aspx




