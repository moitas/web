﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using Shop.CORE;
using Shop.DAL;
using Shop.CORE.Contracts;
using Microsoft.AspNet.Identity;
using Shop.MVC.Models;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using System.Net.Mail;
using Shop.IFR.Tools;
using Shop.IFR.Logging;
using Shop.IFR.MailService;

namespace Shop.MVC.Controllers
{
    [ExceptionHandler]
    [Authorize]
    public class OrdersController : Controller
    {
        IProductManager productManager = null;
        IOrderManager orderManager = null;
        SessionController session = null;

        private ApplicationUserManager _userManager;

        public OrdersController(IProductManager productManager, IOrderManager orderManager)
        {
            this.productManager = productManager;
            this.orderManager = orderManager;
            this.session = new SessionController();
        }

        //*************METODO QUE OFRECE UN LISTADO DE PEDIDOS- PARA EL ROLE ADMIN TOTAL Y PARA EL CLIENT FILTRADO POR IDUSER***************
        /// <summary>
        /// LISTADO PARA PEDIDOS SOLO AUTORIZADOS
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Client, Admin")]
        // GET: Orders
        public ActionResult Index()
        {
            _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            var user = _userManager.FindById(User.Identity.GetUserId());

            if (User.IsInRole("Admin"))
            {
                var oA = orderManager.GetAll().ToList()
                    .Select(e => new OrderList
                    {
                        Id = e.Id,
                        User_Id = e.User_Id,
                        CreatedDate = e.CreatedDate,
                        DateReception = e.DateReception,
                        DateSend = e.DateSend,
                        OrderLines = e.OrderLines,
                        Status = e.Status,
                        Mail = _userManager.FindById(e.User_Id).Email,
                        Total = e.OrderLines.Sum(s => (s.Price * s.Quantity)),
    
                    });

                return View(oA);
            }

            else
            {
                var oC = orderManager.GetByUserId(User.Identity.GetUserId()).ToList()
                    .Select(e => new OrderList
                    {
                        Id = e.Id,
                        User_Id = e.User_Id,
                        CreatedDate = e.CreatedDate,
                        DateReception = e.DateReception,
                        DateSend = e.DateSend,
                        OrderLines = e.OrderLines,
                        Status = e.Status,
                        Total = e.OrderLines.Sum(s => (s.Price * s.Quantity)),
                        Mail = _userManager.FindById(e.User_Id).Email,
                    });
                return View(oC);
            }

        }

        //*******************************************METODOS DE DETALLE DEL PEDIDO - ROLE CLIENTE Y ADMIN*************************************************
        /// <summary>
        /// METODO GET PARA DETALLE DE PEDIDO SOLO AUTORIZADOS
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize]
        // GET: Orders/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Order order = orderManager.GetByIdAndLines(id.Value);///// esta FIJADDOOOOOOOOOOOOOOOOOOOOOOOOOOOOOO

            var ov = new OrderViewModel()
            {
                OrderLinesVM = order.OrderLines.Select(e => new OrderLineViewModel()
                {
                    Id = e.Id,
                    OrderId = e.OrderId,
                    Description = e.Description,
                    Price = e.Price,
                    Quantity = e.Quantity,
                    SubTotal = e.SubTotal,
                    ProductId = e.ProductId,
                }).ToList(),

                Id = order.Id,
                City = order.City,
                Street = order.Street,
                TotalPay= this.session.getCartItems().Sum(s => s.Price * s.Quantity),
        };
            return View(ov);
          
        }

        //*******************************************METODOS DE CREACION DEL PEDIDO -SOLO CLIENT*************************************************************
        /// <summary>
        /// METODO GET PARA CREEACION DEL PEDIDO SOLO ROLE CLIENTE
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Client")]
        // GET: Orders/Create
        public ActionResult Create()
        {
            try
            {
                // Localizar el usuario conectado
                _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = _userManager.FindById(User.Identity.GetUserId());

                if (user == null)
                {
                    throw new Exception("Acceso de usuario sin autenticar a Order.Create.GET");
                }

                //*********** VALORES CARGASO EN LA SESION *******************************************************/////////////////////////////////
                ViewBag.Total = this.session.getCartItems().Sum(s => s.Price * s.Quantity);///cambiar por sesion
                OrderViewModel newOrder = new OrderViewModel
                {
                    //OrderLines = items.ToList(),
                    Status = OrderStatus.Iniciado,
                    CreatedDate = DateTime.Now,
                    User_Id = User.Identity.GetUserId(),
                    CardHolder = user.FirstName + "" + user.LastName,
                    CardNumber = "4143097767715374",
                    City = user.City,
                    Street = user.Address,
                    ZipCode = user.PostalCode,
                    Country = "España",
                    TotalPay = ViewBag.Total,
                    Cvv = "522",
                    ExpirationMonth = "11",
                    ExpirationYear = "2018"
                };

                return View(newOrder);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }

        }

        /// <summary>
        /// METODO POST PATA CREACION DEL PEDIDO SOLO CLIENTE
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        // POST: Orders/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Client")]
        public ActionResult Create(OrderViewModel order)
        {
            if (ModelState.IsValid)
            {
                // Localizar el usuario conectado
                _userManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
                var user = _userManager.FindById(User.Identity.GetUserId());

                if (user == null)
                {
                    throw new Exception("Acceso de usuario sin autenticar a Order.Create.POST");
                }

                // Crear pedido

                if (IsValidCard.IsCreditCardInfoValid(order.CardNumber, order.ExpirationMonth, order.ExpirationYear, order.Cvv) ||
                    order.CardNumber.Equals("9999"))
                {
                    Order newOrder = new Order
                    {
                        Status = OrderStatus.Pagado,
                        CreatedDate = DateTime.Now,
                        User_Id = User.Identity.GetUserId(),
                        CardHolder = order.CardHolder,
                        CardNumber = order.CardNumber,
                        City = order.City,
                        Street = order.Street,
                        ZipCode = order.ZipCode,
                        Country = order.Country,
                        TotalPay = order.TotalPay,
                        OrderLines = this.session.getCartItems()
                           .Select(e => new OrderLine
                           {
                               Description = e.Description,
                               Price = e.Price,
                               Quantity = e.Quantity,
                               User_Id = User.Identity.GetUserId(),
                               ProductId = e.ProductId,
                               SubTotal = e.Price * e.Quantity,
                           }).ToList()
                    };

                    newOrder.TotalPay = newOrder.OrderLines.Sum(s => s.SubTotal);

                    orderManager.Add(newOrder);
                    orderManager.Context.SaveChanges();

                    // Actualizar stocks
                    foreach (var item in this.session.getCartItems())
                    {
                        Product p = this.productManager.GetById(item.ProductId);
                        p.Stock -= item.Quantity;
                    }

                    this.productManager.Context.SaveChanges();

                    // Vaciar carrito
                    this.session.removeAllCartItems();

                    //Envio de mensage
                    MailMessage messageClose = new MailMessage();
                    messageClose.Body = "Ha realizado un nuevo pedido con un importe de " + newOrder.TotalPay + "€ ha sido pagado";
                    SenderMail.Sender(user.Email, messageClose);

                    return RedirectToAction("Index", "Orders");

                }

            }
            else
            {
                ModelState.AddModelError("cardNumber", "Invalid Card");
                return View(order);
            }

            ModelState.AddModelError("bank", "Tarjeta rechazada");
            return View(order);

        }

        //*******************************************METODOS DE EDICION DEL PEDIDO -SOLO ADMIN*******************************************
         /// <summary>
        /// METODO GET PARA EDICION CAMBIO DE ESTADO PEDIDO SOLO ADMIN
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // GET: Orders/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var e = orderManager.GetById(id.Value);

            if (e == null)
            {
                return HttpNotFound();
            }

            OrderViewModel2 o = new OrderViewModel2
            {
                Id = e.Id,
                CreatedDate = e.CreatedDate,
                DateSend = e.DateSend,
                DateReception = e.DateReception,
                Status = e.Status,
                //Total = e.OrderLines.Sum(s => (s.Price * s.Quantity)),
                //TotalPay = e.TotalPay,
                //AddressVM = e.Country + e.Country,
                //OrderLines = e.OrderLines,

            };

            return View(o);

        }

        /// <summary>
        /// METODO POST PARA CAMBIO DE ESTADO PEDIDO SOLO ADMIN
        /// </summary>
        /// <param name="e"></param>
        /// <returns></returns>
        // POST: Orders/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [Authorize(Roles = "Admin")]
        [HttpPost]
        [ValidateAntiForgeryToken]
        //public ActionResult Edit([Bind(Include = "Id,CreatedDate,DateSend,DateReception,User_Id,Status,Street,City,State,Country,ZipCode, TotalPay")] OrderViewModel e)
        public ActionResult Edit(OrderViewModel2 e)
        {
            if (ModelState.IsValid)
            {
                Order o = orderManager.GetById(e.Id);
                
                if (o == null)
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }

                else
                {
                    if (e.Status == OrderStatus.Enviado)
                    {
                        o.DateSend = DateTime.Now;
                        o.DateReception = null;
                    }

                    if (e.Status == OrderStatus.EnProceso)
                    {
                        o.DateSend = null;
                        o.DateReception = null;
                    }

                    if (e.Status == OrderStatus.Recibido)
                    {
                        if (o.DateSend == null)
                        {
                            o.DateSend = DateTime.Now;
                        }
                        o.DateReception = DateTime.Now;
                    }

                    o.Status = e.Status;

                    orderManager.Context.SaveChanges();

                }

                return RedirectToAction("Index");

            }

            return View();
        }

    }
}

