﻿using CallCenterOS.Application;
using CallCenterOS.CORE;
using CallCenterOS.CORE.Contracts;
using CallCenterOS.DAL;
using CallCenterOS.MVC.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CallCenterOS.MVC.Controllers
{
    public class IncidenceController : Controller
    {
        IIncidenceManager incidenceManager = null;

        public IncidenceController(IIncidenceManager incidenceManager)
        {
            this.incidenceManager = incidenceManager;
        }

        // GET: Incidence
        public ActionResult Index()
        {
            var result = incidenceManager.GetByUserId(User.Identity.GetUserId()).Select(e => new IncidenceList
            {
                Id = e.Id,
                Date = e.CreatedDate,
                Message = e.Messages.FirstOrDefault().Text,
                Status = e.Status.ToString()
            });

            return View(result);
        }

        // GET: Incidence/Details/5
        public ActionResult Details(int id)
        {
            var incidence = incidenceManager.GetById(id);
            if (incidence != null)
            {
                if (incidence.User_Id == User.Identity.GetUserId())
                {
                    Models.IncidenceViewModel model = new Models.IncidenceViewModel
                    {
                        Id = incidence.Id,
                        Equipment = incidence.Equipment,
                        CreatedDate = incidence.CreatedDate
                    };
                    return View(model);
                }
            }
            return RedirectToAction("Index");
        }

        // GET: Incidence/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: Incidence/Create
        [HttpPost]
        public ActionResult Create(Models.IncidenceViewModel model)
        {
            try
            {
                CORE.Incidence incidence = new CORE.Incidence
                {
                     Equipment = model.Equipment,
                     CreatedDate = model.CreatedDate,
                     User_Id = User.Identity.GetUserId()
                };
                incidenceManager.Add(incidence);
                incidenceManager.Context.SaveChanges();

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: Incidence/Edit/5
        public ActionResult Edit(int id)
        {
            var incidence = incidenceManager.GetById(id);
            if(incidence!=null)
            {
                if(incidence.User_Id == User.Identity.GetUserId())
                {
                    Models.IncidenceViewModel model = new Models.IncidenceViewModel
                    {
                         Id = incidence.Id,
                         Equipment = incidence.Equipment,
                         CreatedDate = incidence.CreatedDate
                    };
                    return View(model);
                }
            }
            return RedirectToAction("Index");
        }

        // POST: Incidence/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, Models.IncidenceViewModel model)
        {
            try
            {
                var incidence = incidenceManager.GetById(id);
                if(incidence!=null)
                {
                    if(incidence.User_Id == User.Identity.GetUserId())
                    {
                        incidence.Equipment = model.Equipment;
                        incidence.CreatedDate = model.CreatedDate;                        
                        incidenceManager.Context.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                //TODO: Registar el error a traves de IFR
                ModelState.AddModelError("", "Se ha producido un error contacte con el administrador.");
                return View();
            }
        }

        // GET: Incidence/Delete/5
        public ActionResult Delete(int id)
        {
            var incidence = incidenceManager.GetById(id);
            if (incidence != null)
            {
                if (incidence.User_Id == User.Identity.GetUserId())
                {
                    Models.IncidenceViewModel model = new Models.IncidenceViewModel
                    {
                        Id = incidence.Id,
                        Equipment = incidence.Equipment,
                        CreatedDate = incidence.CreatedDate
                    };
                    return View(model);
                }
            }
            return RedirectToAction("Index");
        }

        // POST: Incidence/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, IncidenceViewModel model)
        {
            try
            {
                var incidence = incidenceManager.GetById(id);
                if (incidence != null)
                {
                    if (incidence.User_Id == User.Identity.GetUserId())
                    {
                        incidenceManager.Remove(incidence);
                        incidenceManager.Context.SaveChanges();
                    }
                }

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
