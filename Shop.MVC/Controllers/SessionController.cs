﻿using Shop.CORE;
using Shop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web.Mvc;

namespace Shop.MVC.Controllers
{
    public class SessionController : Controller
    {
        private const string SESSION_KEY = "cart";


        /// <summary>
        /// METODO QUE AÑADE UN CART ITEM A LA SESIÓN
        /// </summary>
        /// <param name="item"></param>
        public void addCartItem(CartItem item)
        {
            Object items = System.Web.HttpContext.Current.Session[SESSION_KEY];

            if (items == null)
            {
                items = new List<CartItem>();

                System.Web.HttpContext.Current.Session[SESSION_KEY] = items;
            }

            if (items is List<CartItem>)
            {
                List<CartItem> cartItems = ((List<CartItem>)items);

                // Buscar el item para el id del producto del nuevo item
                bool found = false;

                for (int i = 0; i < cartItems.Count && !found; i++)
                {
                    if (cartItems[i].ProductId == item.ProductId)
                    {
                        cartItems[i].Quantity += item.Quantity;
                        found = true;
                    }
                }

                if (!found)
                {
                    item.Id = cartItems.Count;
                    ((List<CartItem>)cartItems).Add(item);
                }
            }
        }

        /// <summary>
        /// METODO QUE DEVUELVE LOS ITEMS DEL CARRITO
        /// </summary>
        /// <returns>List<CartItem></returns>
        public List<CartItem> getCartItems()
        {
            Object cartItems = System.Web.HttpContext.Current.Session[SESSION_KEY];

            if (cartItems == null)
            {
                System.Web.HttpContext.Current.Session[SESSION_KEY] = new List<CartItem>();
                return this.getCartItems();
            }

            if (cartItems is List<CartItem>)
            {
                return (List<CartItem>)System.Web.HttpContext.Current.Session[SESSION_KEY];
            }

            return null;
        }

        /// <summary>
        /// METODO QUE UNA LINEA DE ITEMS DEL CARRITO
        /// </summary>
        /// <returns></returns>
        public void removeCartItemByProductId(int productId)
        {
            Object cartItems = System.Web.HttpContext.Current.Session[SESSION_KEY];

            if (cartItems != null)
            {
                bool found = false;

                for (int i = 0; i < ((List<CartItem>)cartItems).Count && !found; i++)
                {
                    if (((List<CartItem>)cartItems)[i].ProductId == productId)
                    {
                        ((List<CartItem>)cartItems).RemoveAt(i);
                        found = true;
                    }
                }

                for (int i = 0; i < ((List<CartItem>)cartItems).Count; i++)
                {
                    ((List<CartItem>)cartItems)[i].Id =i;

                }

            }
        }


        /// <summary>
        /// Metodo que elimina todos los items del carro
        /// </summary>
        public void removeAllCartItems()
        {
            if (this.getCartItems() != null)
            {
                Object cartItems = System.Web.HttpContext.Current.Session[SESSION_KEY];
                ((List<CartItem>)cartItems).Clear();
            }
        }
    }
}