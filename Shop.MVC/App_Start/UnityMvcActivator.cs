using Shop.IFR;
using Shop.IFR.IoC;
using Shop.MVC.Controllers;
using System.Linq;
using System.Web.Mvc;
using Unity.AspNet.Mvc;
using Unity;
using Unity.Injection;


[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Shop.MVC.UnityMvcActivator), nameof(Shop.MVC.UnityMvcActivator.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(Shop.MVC.UnityMvcActivator), nameof(Shop.MVC.UnityMvcActivator.Shutdown))]

namespace Shop.MVC
{
    /// <summary>
    /// Provides the bootstrapping for integrating Unity with ASP.NET MVC.
    /// </summary>
    public static class UnityMvcActivator
    {
        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        public static void Start() 
        {
            UnityConfig.Container.RegisterType<AccountController>(new InjectionConstructor());
            UnityConfig.Container.RegisterType<ManageController>(new InjectionConstructor());

            FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(UnityConfig.Container));

            DependencyResolver.SetResolver(new UnityDependencyResolver(UnityConfig.Container));

            // TODO: Uncomment if you want to use PerRequestLifetimeManager
            // Microsoft.Web.Infrastructure.DynamicModuleHelper.DynamicModuleUtility.RegisterModule(typeof(UnityPerRequestHttpModule));
        }

        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown()
        {
            UnityConfig.Container.Dispose();
        }
    }
}