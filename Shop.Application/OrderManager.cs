﻿using Shop.CORE;
using Shop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Shop.Application
{
    public class OrderManager : GenericManager<CORE.Order>, IOrderManager
    {
        public OrderManager(IApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Método que retorna todas los pedidos de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Todos los pedidos de un usuario</returns>
        public IQueryable<Order> GetByUserId(string userId)
        {
            return Context.Set<Order>().Where(e => e.User_Id == userId);
        }

        /// <summary>
        /// Obtiene una pedido con sus lineas
        /// </summary>
        /// <param name="id">Identificador del pedido</param>
        /// <returns>Pedido con sis lineas si existe o null en caso de no existir</returns>
        public Order GetByIdAndLines(int id)
        {
            return Context.Set<Order>().Include("OrderLines").Where(i => i.Id == id).SingleOrDefault();
        }

        // UTILIZAR ESTE PARA EL DETALLE
        /// <summary>
        /// Metodo que devuelve un pedido por su Id y el usuario
        /// </summary>
        /// <param name="id">Identificador de pedido</param>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Pedido o NULL si no existe</returns>
        ///        
        public Order GetByIdAndUserId(int id, string userId)
        {

            return Context.Set<Order>().Where(i => i.User_Id == userId && i.Id == id).SingleOrDefault();
        }

    }
}
