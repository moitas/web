﻿using Shop.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.DAL;
using Shop.CORE.Contracts;

namespace Shop.Application
{
    /// <summary>
    /// Manager de Message
    /// </summary>
    public class LineManager : GenericManager<OrderLine>, CORE.Contracts.ILineManager
    {
        /// <summary>
        /// Constructor del manager de Item
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public LineManager(IApplicationDbContext context) : base(context)
        {



        }
    }
}
