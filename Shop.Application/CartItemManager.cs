﻿using Shop.CORE;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.DAL;
using Shop.CORE.Contracts;

namespace Shop.Application
{
    /// <summary>
    /// Manager de Message
    /// </summary>
    public class CartItemManager : GenericManager<CartItem>, CORE.Contracts.ICartItemManager
    {
        /// <summary>
        /// Constructor del manager de Item
        /// </summary>
        /// <param name="context">Contexto de datos</param>
        public CartItemManager(IApplicationDbContext context) : base(context)
        {
        }

        /// <summary>
        /// Método que retorna todas las incidencias de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Todas las incidencias de usuario</returns>
        public IQueryable<CartItem> GetByUserId(string userId)
        {
            return Context.Set<CartItem>().Where(e => e.User_Id == userId);
        }



        /// <summary>
        /// Metodo que devuelve un item para un usuario y producto dado
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="productId"></param>
        /// <returns></returns>
        public CartItem GetByProductIdAndCartId(int cartId, int productId)
        {
            return Context.CartItems.Where(i => i.Cart_Id == cartId && i.ProductId == productId).SingleOrDefault();
            //return Context.Set<CartItem>().Where(i => i.Cart_Id == cartId && i.ProductId == productId).FirstOrDefault();

        }




        public void RemoveByUserId(string userId)
        {
            Context.Set<CartItem>().RemoveRange(GetByUserId(userId));
        }
    }
}

