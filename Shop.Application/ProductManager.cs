﻿using Shop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Shop.CORE;


namespace Shop.Application 
{

    public class ProductManager : GenericManager<CORE.Product>, IProductManager
    {
        public ProductManager(IApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Devuelve los productos con stock
        /// </summary>
        /// <returns>Productos con stock</returns>
        public IQueryable<Product> GetListStock()
        {
            return Context.Set<Product>().Where(e => e.Stock>0);

        }

    }
}
