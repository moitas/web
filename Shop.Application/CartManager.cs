﻿using Shop.CORE;
using Shop.CORE.Contracts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Shop.Application
{
    public class CartManager : GenericManager<CORE.Cart>, ICartManager
    {
        public CartManager(IApplicationDbContext context) : base(context)
        {

        }

        /// <summary>
        /// Método que retorna todas los pedidos de un usuario
        /// </summary>
        /// <param name="userId">Identificador de usuario</param>
        /// <returns>Todas las incidencias de usuario</returns>
        public Cart GetByUserId(string userId)
        {
            //return Context.Set<Cart>().Where(e => e.User_Id == userId);
            return Context.Set<Cart>().Include("CartItems").Where(e => e.User_Id == userId).FirstOrDefault();
        }

        /// <summary>
        /// Obtiene una pedido con sus lineas
        /// </summary>
        /// <param name="id">Identificador del pedido</param>
        /// <returns>Pedido con sis lineas si existe o null en caso de no existir</returns>
        public Cart GetByIdAndItems(int id)
        {
            return Context.Set<Cart>().Include("CartItems").Where(i => i.Id == id).SingleOrDefault();
        }



        //public int RemoveAll<T>(this ICollection<T> collection, Predicate<T> predicate)
        //{
        //    if (predicate == null)
        //    {
        //        throw new ArgumentNullException("predicate");
        //    }
        //    collection.Where(entity => predicate(entity))
        //       .ToList().ForEach(entity => collection.Remove(entity));
        //}



        //public Order GetByIdWithItems(int id)
        //{
        //    return _dbContext.Orders
        //        .Include(o => o.OrderItems)
        //        .Include("OrderItems.ItemOrdered")
        //        .FirstOrDefault();
        //}

        //public Task<Order> GetByIdWithItemsAsync(int id)
        //{
        //    return _dbContext.Orders
        //        .Include(o => o.OrderItems)
        //        .Include("OrderItems.ItemOrdered")
        //        .FirstOrDefaultAsync();
        //}


    }
}
