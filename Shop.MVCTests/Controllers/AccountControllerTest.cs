﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Web.Mvc;
using System.Collections.Specialized;
using System.Globalization;
using Shop.MVC;
using Shop.MVC.Controllers;
using Shop.MVC.Models;
namespace Shop.MVCTests.Controllers
{
    /// <summary>
    /// Unit tests for account controller
    /// </summary>
    [TestClass]
    public class AccountControllerTest
    {

        /// <summary>
        /// Tests the GET for register.
        /// </summary>
        [TestMethod]
        public void TestRegisterGet()
        {
            var accountController = new AccountController();
            var result = accountController.Register();
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }


        [TestMethod]
        public void TestAccountLoginGet()
        {
            var accountController = new AccountController();
            var result = accountController.Login(null);
            Assert.IsInstanceOfType(result, typeof(ViewResult));
        }



    }
}
