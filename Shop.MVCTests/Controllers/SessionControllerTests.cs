﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.Linq;
using Shop.CORE;
using System.Collections.Generic;

namespace Shop.MVCTests.Controllers
{
    [TestClass]
    public class SessionControllerTests
    {

        [TestMethod]
        public void GetCartItemsTest()
        {
            List<CartItem> sessions = new List<CartItem>();
            sessions.Add(new CartItem()
            {
                Description = "d",
                ProductId = 1
            });
            sessions.Add(new CartItem()
            {
                Description = "d",
                ProductId = 1
            });


            //Act

            var result = sessions.Count();

            //Assert
            Assert.IsNotNull(result);
        }

    }
}
