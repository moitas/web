﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shop.CORE;

using Shop.CORE.Contracts;
using Shop.MVC.Controllers;
using Shop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;

using System.Data;
using System.Data.Entity;

using System.Net;
using System.Web;
using Shop.MVC.Models;
using System.IO;
using Shop.IFR.Tools;
using Microsoft.AspNet.Identity;
using Shop.IFR.Logging;
using Shop.MVCTests.Controllers;

namespace Shop.MVC.Controllers.Tests
{


    [TestClass]
    public class OrdersControllerTests
    {
        IProductManager productManager = null;
        IOrderManager orderManager = null;
        SessionController session = null;

        private ApplicationUserManager _userManager;


        /// <summary>
        /// Deniega el acceso al listado de pedidos sin autorizacion?
        /// </summary>
        [TestMethod]
        public void IndexOrder_IsAnonymousInOrderFalse()
        {
            // Arrange
            OrdersController controller = new OrdersController(productManager, orderManager);

            // Assert
            Assert.IsFalse(AuthorizationTest.IsAnonymous(
                controller,
                "Index",
                null));
        }

        [TestMethod]
        public void DetailsOrder_IsAuthorized()
        {
            // Arrange
            OrdersController controller = new OrdersController(productManager, orderManager);

            // Asset
            Assert.IsTrue(AuthorizationTest.IsAuthorized(
                controller,
                "Details",
                new Type[] { typeof(int) }));
        }

        [TestMethod]
        public void EditOrder_IsAuthorized()
        {
            // Arrange
            OrdersController controller = new OrdersController(productManager, orderManager);

            // Asset
            Assert.IsTrue(AuthorizationTest.IsAuthorized(
                controller,
                "Edit",
                new Type[] { typeof(int) }));
        }


        [TestMethod]
        public void CreateOrder_Get_IsAuthorized()
        {
            // Arrange
            OrdersController controller = new OrdersController(productManager, orderManager);

            // Assert
            Assert.IsFalse(AuthorizationTest.IsAuthorized(
                controller,
                "Create",
                null,
                new string[] { "Admin" },
                new string[] { "Ross" }));
        }



    }
}