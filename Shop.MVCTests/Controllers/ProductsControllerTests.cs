﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shop.CORE;

using Shop.CORE.Contracts;
using Shop.MVC.Controllers;
using Shop.MVC.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using System.Net;
using System.Web;

using System.IO;
using Shop.IFR.Tools;
using Microsoft.AspNet.Identity;
using Shop.IFR.Logging;
using System.Security.Principal;
using Shop.MVCTests.Controllers;


namespace Shop.MVC.Controllers.Tests
{

    [TestClass()]
    public class ProductsControllerTests
    {
        private MockRepository mockRepository;

        private Mock<IProductManager> mockProductManager;

        //[SetUp]
        //public void SetUp()
        //{
        //    this.mockRepository = new MockRepository(MockBehavior.Strict);

        //    this.mockProductManager = this.mockRepository.Create<IProductManager>();
        //}

        //[TearDown]
        //public void TearDown()
        //{
        //    this.mockRepository.VerifyAll();
        //}

        IProductManager productManager = null;


        //https://stackoverflow.com/questions/22762338/how-do-i-mock-user-identity-getuserid

        [TestMethod]
        public void ProductControllerTest()
        {

            // Arrange
            var mockRepository = new Mock<IProductManager>();

            var ProductList = new List<Product>
            {
                new Product() { ProductId = 1, Description="uno", Price=6, Stock=5 },
                new Product() { ProductId = 2,  Description="dos", Price=4, Stock=5},
              new Product() { ProductId = 3,  Description="tres", Price=4, Stock=0}
            }.AsQueryable();


            mockRepository
                .Setup(x => x.GetAll())
                .Returns(ProductList);
            var controller = new ProductsController(mockRepository.Object);

            //Act

            var result = controller.Index();

            //Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual(ProductList, response);
            //mockRepository.VerifyAll();
        }


        [TestMethod]
        public void ProductControllerTest2()
        {
            var ProductList = new List<Product>
            {
                new Product() { ProductId = 1, Description="uno", Price=6, Stock=5 },
                new Product() { ProductId = 2,  Description="dos", Price=4, Stock=5},
              new Product() { ProductId = 3,  Description="tres", Price=4, Stock=0}
            }.AsQueryable();

            var mockRepository = new Mock<IProductManager>();

            mockRepository
                .Setup(x => x.GetById(1))
                .Returns(ProductList.Where(p => p.ProductId == 1).FirstOrDefault());

            var controller = new ProductsController(mockRepository.Object);

            //Act

            var result = controller.Details(1);

            //Assert
            Assert.IsNotNull(result);
            //Assert.AreEqual(ProductList, response);
            //mockRepository.VerifyAll();
        }


        [TestMethod]
        public void TestProductCreateViewView()
        {

            //Arrange
            var controller = new ProductsController(productManager);

            var product = new ProductCreateViewModel()
            {
                ProductId = 999,
                Description = "dos",
                Price = 4,
                Stock = 5
            };

            var result = controller.Create(product);

            Assert.IsNotNull(result);
        }


        /// <summary>
        /// Deniega el acceso al listado de pedidos sin autorizacion?
        /// </summary>
        [TestMethod]
        public void IndexProduct_IsAnonymousInProductTrue()
        {
            // Arrange
            ProductsController controller = new ProductsController(productManager);

            // Assert
            Assert.IsTrue(AuthorizationTest.IsAnonymous(
                controller,
                "Index",
                null));
        }

        [TestMethod]
        public void DetailsProduct_IsAnonymousInProduct()
        {
            // Arrange
            ProductsController controller = new ProductsController(productManager);

            // Asset
            Assert.IsTrue(AuthorizationTest.IsAnonymous(
                controller,
                "Details",
                new Type[] { typeof(int) }));
        }


        [TestMethod]
        public void EditProduct_IsAuthorizedInProduct()
        {
            // Arrange
            ProductsController controller = new ProductsController(productManager);

            // Asset
            Assert.IsTrue(AuthorizationTest.IsAuthorized(
                controller,
                "Edit",
                new Type[] { typeof(int) }));
        }


        [TestMethod]
        public void CreateProduct_Get_IsAuthorizedInProduct()
        {
            // Arrange
            ProductsController controller = new ProductsController(productManager);

            // Assert
            Assert.IsFalse(AuthorizationTest.IsAuthorized(
                controller,
                "Create",
                null,
                new string[] { "Admin" },
                new string[] { "Ross" }));
        }
    }
}





