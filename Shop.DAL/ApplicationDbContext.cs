﻿using Shop.CORE;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Shop.CORE.Contracts;
using Shop.DAL.Migrations;

namespace Shop.DAL
{
    /// <summary>
    /// Contexto de datos
    /// </summary>
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>, IApplicationDbContext
    {
        /// <summary>
        /// Constructor por defecto de la clase
        /// </summary>
        public ApplicationDbContext()
            : base("DefaultConnection", throwIfV1Schema: false)
        {
        }
        /// <summary>
        /// Método estatico para crear el contexto
        /// </summary>
        /// <returns>Contexto de datos</returns>
        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            Database.SetInitializer(new
                MigrateDatabaseToLatestVersion<ApplicationDbContext, Configuration>());
            base.OnModelCreating(modelBuilder);
        }



        /// <summary>
        /// Colección persistible de mensajes
        /// </summary>
        //public DbSet<OrderLine> OrderLines { get; set; }


        /// <summary>
        /// Colección persistible de incidencias
        /// </summary>
        public DbSet<Order> Orders { get; set; }


        /// <summary>
        /// Colección persistible de productos
        /// </summary>
        public DbSet<Product> Products { get; set; }

        /// <summary>
        /// Colección persistible de excepciones
        /// </summary>
         public DbSet<ExceptionLogger> ExceptionLoggers { get; set; }


        ///// <summary>
        ///// Colección persistible de productos
        ///// </summary>
        //public DbSet<Cart> Carts { get; set; }


        ///// <summary>
        ///// Colección persistible de productos
        ///// </summary>
        //public DbSet<CartItem> CartItems { get; set; }


        //public DbSet<Address> Addresss { get; set; }

        //public System.Data.Entity.DbSet<Shop.MVC.Models.UserListViewModel> UserListViewModels { get; set; }

        //public System.Data.Entity.DbSet<Shop.CORE.ApplicationUser> ApplicationUsers { get; set; }

        //public System.Data.Entity.DbSet<ApplicationRoles> Roles { get; set; }

    }
}
