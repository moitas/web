﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NSubstitute;
using Shop.CORE;
using Shop.CORE.Contracts;
using Shop.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;



namespace Shop.Application.Tests
{
    [TestClass]
    public class OrderManagerTest
    {
        IGenericManager<Order> repository;
        IOrderManager repository2;
        Mock<IOrderManager> mockOrderRepository = new Mock<IOrderManager>();

        [TestInitialize]
        public void Init()
        {

            IList<Order> orders = new List<Order>
            {
                new Order() { Id = 1, Status = OrderStatus.Pagado, User_Id="1", CreatedDate= DateTime.Now,CardHolder="wñdk", Street="fdsfdd", TotalPay=50  },
                new Order() { Id = 2, Status = OrderStatus.Pagado, User_Id="1", CreatedDate= DateTime.Now,CardHolder="wñdk", Street="fdsfdd", TotalPay=50  }

            };

            var moqRepository = new Mock<IGenericManager<Order>>();
            moqRepository.Setup(moq => moq.GetAll()).Returns((orders.AsQueryable));
            repository = moqRepository.Object;

            // return a product by Id
            var moqRepositoryP = new Mock<IOrderManager>();
            moqRepositoryP.Setup(moq => moq.GetById(It.IsAny<int>())).Returns((int i) => orders.Where(x => x.Id == i).Single());
            repository2 = moqRepositoryP.Object;

        }



        /// <summary>
        /// Test Cuenta los registros añadidos a la coleccion de productos usando contratos
        /// </summary>
        [TestMethod]
        public void OrderCountRegisterTestWithIterfaceTest()
        {
            var cuenta = repository.GetAll().Count();
            Assert.AreEqual(2, cuenta);
        }

        /// <summary>
        /// Test - Comprueba getbyid a traves de contratos
        /// </summary>
        [TestMethod]
        public void OrderGetByIdTest()
        {
            var order = repository2.GetById(1);
            Assert.AreEqual(1, order.Id);
        }


    }
}
