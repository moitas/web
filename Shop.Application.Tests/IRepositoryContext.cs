﻿using Shop.CORE;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.Application.Tests
{
    class IRepositoryContext
    {
        public interface IRepositoryContext
        {
            IDbSet<Product> Products { get; set; }
        }

    }
}
