﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using NSubstitute;
using Shop.CORE;
using Shop.CORE.Contracts;
using Shop.DAL;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace Shop.Application.Tests
{
    [TestClass()]
    public class ProductManagerTests
    {

        IGenericManager<Product> repository;
        IProductManager repository2;

        Mock<IProductManager> mockProductRepository = new Mock<IProductManager>();

        [TestInitialize]

        public void Init()
        {
            IList<Product> products = new List<Product>
            {
                        new Product { ProductId = 1, DescriptionLarge = "Uno largo",
                        Description = "Uno corto", Price = 49,Stock=3 },
                        new Product { ProductId = 3, DescriptionLarge = "C# Unleashed",
                        Description = "Short description here", Price = 50,Stock=0 },
            };

            var moqRepository = new Mock<IGenericManager<Product>>();
            //var moqRepository = new Mock<IProductManager>();
            moqRepository.Setup(moq => moq.GetAll()).Returns(products.AsQueryable);
            repository = moqRepository.Object;

            // return a product by Id
            //var moqRepository = new Mock<IProductManager>();
            var moqRepositoryP = new Mock<IProductManager>();
            moqRepositoryP.Setup(moq => moq.GetById(It.IsAny<int>())).Returns((int i) => products.Where(x => x.ProductId == i).Single());
            repository2 = moqRepositoryP.Object;
   
        }

        /// <summary>
        /// Test Cuenta los registros añadidos a la coleccion de productos usando contratos
        /// </summary>
    [TestMethod()]
        public void ProductCountRegisterTestWithIterfaceTest()

        {
            var cuenta = repository.GetAll().Count();
            Assert.AreEqual(2, cuenta);            
        }

        /// <summary>
        /// Test - Comprueba getbyid a traves de contratos
        /// </summary>
        [TestMethod()]
        public void ProductGetByIdTest()

        {
            var pr = repository2.GetById(1);
            Assert.AreEqual(1, pr.ProductId);
        }

    }


}

