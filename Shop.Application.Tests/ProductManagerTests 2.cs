﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using Shop.CORE;
using Shop.CORE.Contracts;
using Shop.DAL;
using System.Collections.Generic;
using System.Linq;

namespace Shop.Application.Tests
{
    [TestClass()]
    public class ProductManagerTests2
    {
        private ApplicationDbContext dbContext;
        ProductManager p;


        public ProductManagerTests()
        {
            ApplicationDbContext dbContext = new ApplicationDbContext();
            ProductManager p = new ProductManager(dbContext);

        }

        [TestMethod()]
        public void GetAll()
        {
            // this is the mocked data contained in your mocked DbContext
            var products = new List<Product>(){
                  new Product(){ ProductId = 27, Description = "p1", DescriptionLarge = "p1"},
                  new Product(){Description = "p2", DescriptionLarge = "p2"},
                  new Product(){Description = "p3", DescriptionLarge = "p3"},
                  new Product(){Description = "p4", DescriptionLarge = "p4"},
                  /*and more if needed*/
                };
            // Create a fake/Mocked DbContext
            var mockedContext = NSubstitute.Substitute.For<ApplicationDbContext>();
            // call to extension method which mocks the DbSet and adds it to the DbContext
            mockedContext.AddToDbSet(products);

            // create your repository that you want to test and pass in the fake DbContext
            var manager = new ProductManager(mockedContext);

            // act
            var results = manager.GetAll();

            // assert
            Assert.AreEqual(results.Count(), products.Count);
        }

        [TestMethod()]
        public void GetById()
        {

            var product = new Product()
            {

                Description = "uno xxxx",
                DescriptionLarge = "desc. l xxxxxxxxxxxxx",
                Price = 2,
                Stock = 3,

            };

            var business = new ProductManager(new ApplicationDbContext());

            var p = business.Add(product);
            business.Context.SaveChanges();

            var lista = business.GetAll().FirstOrDefault();

            //var repositoryMock = new Mock<IProductManager>();

            //repositoryMock.
            //repositoryMock.Setup(x=>x.s)

            //mockDataAccess.Setup(m => m.Add(It.IsAny<Product>()));
            //var productBusiness = new ProductManager(mockDataAccess.Object);





            //// Act
            //Product p = productManager.GetById(0);
            //var products = productManager.GetAll();

            // Assert
            Assert.AreEqual("uno xxxx", product.Description);
        }
    }
}