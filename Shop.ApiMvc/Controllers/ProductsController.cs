﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Shop.ApiMvc.Models;
using Shop.CORE;
using Shop.DAL;
using Shop.CORE.Contracts;

namespace Shop.ApiMvc.Controllers
{
    public class ProductsController : ApiController
    {
 
        private IProductManager productManager = null;

        public ProductsController(IProductManager productManager)
        {
            this.productManager = productManager;
        }

        /// <summary>
        /// Metodo que devuelve una lista completa de productos
        /// </summary>
        /// <returns></returns>
        // GET: api/Products
        public IQueryable<Product> GetProducts()
        {

            var result = productManager.GetAll();



            return (result);
        }

        /// <summary>
        /// Metodo que devuelve un producto por su identificador
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        //// GET: api/Products/5
        [ResponseType(typeof(Product))]
        public IHttpActionResult GetProduct(int id)
        {
            Product product = productManager.GetById(id);
            if (product == null)
            {
                return NotFound();
            }

            return Ok(product);
        }

        [Authorize(Roles = "Admin")]
        /// <summary>
        /// Metodo que modifica un producto
        /// </summary>
        /// <param name="id"></param>
        /// <param name="product"></param>
        /// <returns></returns>
        //// PUT: api/Products/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutProduct(int id, Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != product.ProductId)
            {
                return BadRequest();
            }

            productManager.Context.Entry(product).State = EntityState.Modified;

            try
            {
                productManager.Context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProductExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authorize(Roles = "Admin")]
        /// <summary>
        /// Metodo que crea un producto
        /// </summary>
        /// <param name="product"></param>
        /// <returns></returns>
        //// POST: api/Products
        [ResponseType(typeof(Product))]
        public IHttpActionResult PostProduct(Product product)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            productManager.Add(product);
            productManager.Context.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = product.ProductId }, product);
        }


        private bool ProductExists(int id)
        {
            return productManager.GetAll().Count(e => e.ProductId == id) > 0;
        }

    }
}


