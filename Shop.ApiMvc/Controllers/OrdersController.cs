﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using Shop.CORE;
using Shop.DAL;
using Shop.CORE.Contracts;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System.Web;



namespace Shop.ApiMvc.Controllers
{
    [Authorize(Roles = "Client, Admin")]
    public class OrdersController : ApiController
    {
        private IOrderManager orderManager = null;
        private ApplicationUserManager _userManager;

        public OrdersController(IOrderManager orderManager)
        {
            this.orderManager = orderManager;
        }


        /// <summary>
        /// Metodo Api que devuelve todos los pedidos
        /// </summary>
        /// <returns></returns>
        [Authorize(Roles = "Admin")]
        // GET: api/Orders
        public IQueryable<Order> GetOrders()
        {
            var result = orderManager.GetAll().Include(e => e.OrderLines);
            return (result);
        }


        [Authorize(Roles = "Admin")]
        /// <summary>
        /// Metodo Api que devuelve 1 pedido pasandole el id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        // GET: api/Orders/5
        [ResponseType(typeof(Order))]
        public IHttpActionResult GetOrder(int id)
        {
            Order order = orderManager.GetById(id);
            if (order == null)
            {
                return NotFound();
            }

            return Ok(order);
        }

        [Authorize(Roles = "Admin")]
        /// <summary>
        /// Metodo Api que actualiza un pedido
        /// </summary>
        /// <param name="id"></param>
        /// <param name="order"></param>
        /// <returns></returns>
        // PUT: api/Orders/5
        [ResponseType(typeof(void))]
        public IHttpActionResult PutOrder(int id, Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != order.Id)
            {
                return BadRequest();
            }

            orderManager.Context.Entry(order).State = EntityState.Modified;

            try
            {
                orderManager.Context.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!OrderExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        [Authorize(Roles = "Client")]
        /// <summary>
        /// Metodo Api que crea un pedido
        /// </summary>
        /// <param name="order"></param>
        /// <returns></returns>
        // POST: api/Orders
        [ResponseType(typeof(Order))]
        public IHttpActionResult PostOrder(Order order)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            orderManager.Add(order);
            orderManager.Context.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = order.Id }, order);
        }

        private bool OrderExists(int id)
        {
            return orderManager.GetAll().Count(e => e.Id == id) > 0;
        }
    }
}