using System.Web.Http;
using Unity;
using Unity.WebApi;
using Shop.CORE.Contracts;
using System;

namespace Shop.ApiMvc
{
    public static class UnityConfig
    {
        public static void RegisterComponents()
        {
			var container = new UnityContainer();
            
            // register all your components with the container here
            // it is NOT necessary to register your controllers
            
            // e.g. container.RegisterType<ITestService, TestService>();
            
            GlobalConfiguration.Configuration.DependencyResolver = new UnityDependencyResolver(container);


            //https://www.devtrends.co.uk/blog/using-unity.mvc5-and-unity.webapi-together-in-a-project
            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IApplicationDbContext, Shop.CORE"), Type.GetType("Shop.DAL.ApplicationDbContext, Shop.DAL"), new PerRequestLifetimeManager());

            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ILineManager, Shop.CORE"), Type.GetType("Shop.Application.LineManager, Shop.Application"));
            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IOrderManager, Shop.CORE"), Type.GetType("Shop.Application.OrderManager, Shop.Application"));
            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IProductManager, Shop.CORE"), Type.GetType("Shop.Application.ProductManager, Shop.Application"));

            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IExceptionLoggers, Shop.CORE"), Type.GetType("Shop.Application.ExceptionLoggers, Shop.Application"), "loggers");

            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ICartManager, Shop.CORE"), Type.GetType("Shop.Application.CartManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ICartItemManager, Shop.CORE"), Type.GetType("Shop.Application.CartItemManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IAddressManager, Shop.CORE"), Type.GetType("Shop.Application.AddressManager, Shop.Application"));


        }
    }
}