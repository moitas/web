using Shop.ApiMvc.Controllers;
using Shop.IFR.IoC;
using System.Web.Http;

using Unity.AspNet.WebApi;

using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Unity;
using Shop.ApiMvc.Controllers;
using Unity.Injection;
using Shop.IFR.IoC;

[assembly: WebActivatorEx.PreApplicationStartMethod(typeof(Shop.ApiMvc.UnityWebApiActivator), nameof(Shop.ApiMvc.UnityWebApiActivator.Start))]
[assembly: WebActivatorEx.ApplicationShutdownMethod(typeof(Shop.ApiMvc.UnityWebApiActivator), nameof(Shop.ApiMvc.UnityWebApiActivator.Shutdown))]

namespace Shop.ApiMvc
{
    /// <summary>
    /// Provides the bootstrapping for integrating Unity with WebApi when it is hosted in ASP.NET.
    /// </summary>
    public static class UnityWebApiActivator
    {
        /// <summary>
        /// Integrates Unity when the application starts.
        /// </summary>
        public static void Start() 
        {
            // Use UnityHierarchicalDependencyResolver if you want to use
            // a new child container for each IHttpController resolution.
            // var resolver = new UnityHierarchicalDependencyResolver(UnityConfig.Container);
            //var resolver = new UnityDependencyResolver(UnityConfig.Container);

            //GlobalConfiguration.Configuration.DependencyResolver = resolver;




        }

        /// <summary>
        /// Disposes the Unity container when the application is shut down.
        /// </summary>
        public static void Shutdown()
        {
            UnityConfig.Container.Dispose();
        }
    }
}