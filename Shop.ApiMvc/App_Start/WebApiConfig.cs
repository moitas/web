﻿using System.Web.Http;
using Microsoft.Owin.Security.OAuth;
using Unity;
using Shop.ApiMvc.Controllers;
using Unity.Injection;
using Shop.IFR.IoC;

namespace Shop.ApiMvc
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            UnityConfig.Container.RegisterType<AccountController>(new InjectionConstructor());

            //FilterProviders.Providers.Remove(FilterProviders.Providers.OfType<FilterAttributeFilterProvider>().First());
            //FilterProviders.Providers.Add(new UnityFilterAttributeFilterProvider(UnityConfig.Container));

            config.DependencyResolver = new UnityResolver(UnityConfig.Container);

            // Web API configuration and services
            // Configurar inyección de dependencias
            //var container = new UnityContainer();
            //container.RegisterType<AccountController>(new InjectionConstructor());
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IApplicationDbContext, Shop.CORE"), Type.GetType("Shop.DAL.ApplicationDbContext, Shop.DAL"));

            ////container.RegisterType(Type.GetType("Shop.CORE.Contracts.ILineManager, Shop.CORE"), Type.GetType("Shop.Application.LineManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IOrderManager, Shop.CORE"), Type.GetType("Shop.Application.OrderManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IProductManager, Shop.CORE"), Type.GetType("Shop.Application.ProductManager, Shop.Application"));

            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IExceptionLoggers, Shop.CORE"), Type.GetType("Shop.Application.ExceptionLoggers, Shop.Application"), "loggers");

            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));


            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );
        }
    }
}
