﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Shop.CORE;


namespace Shop.CORE.Test
{
    [TestClass]
    public class UnitTestProduct
    {

        [TestMethod]
        public void Two_references_to_same_transient_entity_should_be_equal()
        {

            var p1 = new Product();
            var p2 = p1;

            Assert.AreEqual(p1, p2, "Two references to the same transient entity should be equal");
        }



        [TestMethod]
        public void Entities_with_different_id_should_not_be_equal()
        {

            var p1 = new Product { ProductId = 2 };
            var p2 = new Product { ProductId = 5 };

            Assert.AreNotEqual(p1, p2, "Entities with different ids should not be equal");
        }

        [TestMethod]
        public void Entity_should_not_equal_transient_entity()
        {

            var p1 = new Product { ProductId = 1 };
            var p2 = new Product();

            Assert.AreNotEqual(p1, p2, "Entity and transient entity should not be equal");
        }


    }
}
