﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Shop.CORE
{
    public class Address
    {
        ///// <summary>
        ///// Identificador de la compra
        ///// </summary>
        ///// 
        [Key]
        public int Id { get; set; }

        //[Required]
        public string UserName{ get; set; }


        /// <summary>
        /// Codigo postal
        /// </summary>
        /// 
        //[Required]
        [Display(Name = "Postal Code")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Ciudad
        /// </summary>
        /// 
        [Display(Name = "Ciudad")]
        public string City { get; set; }


        /// <summary>
        /// Pais
        /// </summary>
        /// 
        [Display(Name = "País")]
        public string Country { get; set; }


        /// <summary>
        /// Dirección del usuario
        /// </summary>
        public string Road { get; set; }



        /// <summary>
        /// Usuario que ha creado la incidencia
        /// </summary>
        /// 

        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que ha realizado la compra
        /// </summary>
        ///         
        [ForeignKey("User")]
        public string User_Id { get; set; }


    }
}
