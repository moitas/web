﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Shop.CORE
{
    public class Cart
    {

        /// <summary>
        /// Identificador de la carro
        /// </summary>
        /// 
        public int Id { get; set; }


        /// <summary>
        /// Usuario que ha creado la incidencia
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que ha realizado la compra
        /// </summary>5
        public string User_Id { get; set; }
        

        ////[Index("TitleIndex", IsUnique = true)]
        ///// <summary>
        ///// Identificador del usuario que ha realizado la compra
        ///// </summary>
        //public string User_Id { get; set; }


        /// <summary>
        /// Colección de productos
        /// </summary>
        public virtual List<CartItem> CartItems { get; set; }



    }
}
