﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Shop.CORE
{
    public class CartItem
    {
        /// <summary>
        /// Identificador del item
        /// </summary>
        /// 
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        /// 
        [Required(ErrorMessage = "Nada de anónimos")]
        [Display(Name = "Descripcion")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto actual
        /// </summary>
        /// 
        [Display(Name = "Precio(€)")]
        //[DataType(DataType.Currency)]
        [Required(ErrorMessage = "Verifica el precio entre 0-999")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        //[DisplayFormat(DataFormatString = "{0:C0}")]
        public decimal Price { get; set; }

        /// <summary>
        /// Cantidad del prodcuto
        /// </summary>
        /// 
        [Required(ErrorMessage = "Verifica la cantidad")]
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }

        /// <summary>
        /// Usuario que crea la linea
        /// </summary>
        public ApplicationUser User { get; set; }
        
        /// <summary>
        /// Identificador del usuario
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }


        /// <summary>
        /// Carro a la que pertenece el item
        /// </summary>
        public Cart Cart { get; set; }

        /// <summary>
        /// Identificador del carro a la que pertenece el item
        /// </summary>
        [ForeignKey("Cart")]
        public int Cart_Id { get; set; }


        /// <summary>
        /// Prodiucto  eliminacion en cascada
        /// </summary>
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        /// Producto que trata la linea
        /// </summary>
        public Product Product { get; set; }



    }
}
