﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Shop.CORE
{
    public class Order
    {
        /// <summary>
        /// Identificador de la compra
        /// </summary>
        /// 
        [Key]
        public int Id { get; set; }

        /// <summary>
        /// Fecha en la que se ha realizado la compra
        /// </summary>
        /// 
        [Required]
        [Display(Name = "Fecha de creación")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime CreatedDate { get; set; }

        /// <summary>
        /// Fecha de envio
        /// </summary>
        /// 
        [Display(Name = "Fecha de envio")]
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime? DateSend { get; set; }

        /// <summary>
        /// Fecha de recepcion
        /// </summary>
        /// 
        [Display(Name = "Fecha de recepción")]// abbreviation shown in Html.DisplayNameFor
        [DataType(DataType.Date)]
        [DisplayFormat(DataFormatString = "{0:yyyy/MM/dd}", ApplyFormatInEditMode = true)]  // format used by Html.EditorFor
        public DateTime? DateReception { get; set; }

        /// <summary>
        /// Usuario que ha creado la incidencia
        /// </summary>
        public ApplicationUser User { get; set; }

        /// <summary>
        /// Identificador del usuario que ha realizado la compra
        /// </summary>5
        public string User_Id { get; set; }


        /// <summary>
        /// Estado del pedido (Enumerado)
        /// </summary>
        public OrderStatus Status { get; set; }

        /// <summary>
        /// Colección de mensajes
        /// </summary>
        public virtual List<OrderLine> OrderLines { get; set; }

        /// <summary>
        /// Calle del entrega
        /// </summary>
        [Required]
        public String Street { get; set; }

        /// <summary>
        /// Ciudad de entrega
        /// </summary>
        [Required]
        public String City { get; set; }

        /// <summary>
        /// Region donde se entregará el pedido
        /// </summary>
        public String State { get; set; }

        /// <summary>
        /// Pais donde se entregará el pedido
        /// </summary>
        public String Country { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        public String ZipCode { get; set; }

        /// <summary>
        /// Numero de tarjeta de credito
        /// </summary>
        [RegularExpression(@"^(?:4[0-9]{12}(?:[0-9]{3})?|[25][1-7][0-9]{14}|6(?:011|5[0-9][0-9])[0-9]{12}|3[47][0-9]{13}|3(?:0[0-5]|[68][0-9])[0-9]{11}|(?:2131|1800|35\d{3})\d{11})$", ErrorMessage = "Tarjeta no valida"), Required]
        public string CardNumber { get; set; }

        /// <summary>
        /// Propietario de la tarjeta de credito con la que se realiza el pago
        /// </summary>
        [Required]
        public string CardHolder { get; set; }

        /// <summary>
        /// Total pagado con la tarjeta
        /// </summary>
        [Display(Name = "Total a pagar(€)")]
        [Required(ErrorMessage = "Importe no valido")]
        [Range(1, 9999, ErrorMessage = "Coste de pedido máximo. Entre de 1 9999")]
        public decimal TotalPay { get; set; }

    }
}
