﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;


namespace Shop.CORE
{
    /// <summary>
    /// Entidad de dominio usuario
    /// </summary>
    public class ApplicationUser : IdentityUser
    {
        public ClaimsIdentity GenerateUserIdentity(UserManager<ApplicationUser> manager)
        {
            // Tenga en cuenta que authenticationType debe coincidir con el valor definido en CookieAuthenticationOptions.AuthenticationType
            var userIdentity = manager.CreateIdentity(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Agregar reclamaciones de usuario personalizadas aquí
            return userIdentity;
        }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager, string authenticationType)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Add custom user claims here
            return userIdentity;
        }

        public Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            return Task.FromResult(GenerateUserIdentity(manager));
        }

        /// <summary>
        /// Nombre del usuario registrado
        /// </summary>
        /// 
        [Display(Name = "Nombre")]
        [Required(ErrorMessage = "Nombre requerido")]
        [StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        public string FirstName { get; set; }


        [Display(Name = "Apellido")]
        [Required(ErrorMessage = "Apellido requerido")]
        [StringLength(30, ErrorMessage = "Name may not be longer than 30 characters")]
        public string LastName { get; set; }

        /// <summary>
        /// Codigo postal
        /// </summary>
        /// 
        [Required(ErrorMessage = "Codigo postal requerido")]
        [Display(Name = "Postal Code")]
        [DataType(DataType.PostalCode)]
        public string PostalCode { get; set; }

        /// <summary>
        /// Ciudad
        /// </summary>
        /// 
        [Display(Name = "Ciudad")]
        [Required(ErrorMessage = "Ciudad requerida")]
        public string City { get; set; }


        /// <summary>
        /// Ciudad
        /// </summary>
        /// 
        [Required(ErrorMessage = "Direccion requerida")]
        [Display(Name = "Dirección")]
        public string Address { get; set; }



        //public string UserRole { get; set; }

        //public virtual Address Address { get; set; }

        //public virtual Cart Cart { get; set; }


        //// Concatenate the address info for display in tables and such:
        //public string DisplayAddress { get; set; }
        //{
        //    get
        //    {
        //        string dspAddress = string.IsNullOrWhiteSpace(this.Address) ? "" : this.Address;
        //        string dspCity = string.IsNullOrWhiteSpace(this.City) ? "" : this.City;
        //        string dspState = string.IsNullOrWhiteSpace(this.Country) ? "" : this.Country;
        //        string dspPostalCode = string.IsNullOrWhiteSpace(this.PostalCode) ? "" : this.PostalCode;

        //        return string.Format("{0} {1} {2} {3}", dspAddress, dspCity, dspState, dspPostalCode);
        //    }
        //}


        //[DataType(DataType.CreditCard)]
        //[Display(Name = "Credit Card Number")]
        //[Required(ErrorMessage = "required")]
        //[Range(100000000000, 9999999999999999999, ErrorMessage = "must be between 12 and 19 digits")]
        //public long CardNumber { get; set; }


        // public virtual Cart Cart { get; set; }

    }
}
