﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/// <summary>
/// Estado del pedido
/// </summary>
namespace Shop.CORE
{
    /// <summary>
    /// Estado de pedido 
    /// </summary>
    public enum OrderStatus
    {
        /// <summary>
        /// Pedido creado
        /// </summary>
        Iniciado=0,
        /// <summary>
        /// Pedido creado y pagado
        /// </summary>
        Pagado = 1,
        /// <summary>
        /// Pedido en proceso de preparación
        /// </summary>
        EnProceso = 2,
        /// <summary>
        /// Pedido ya enviado
        /// </summary>
        Enviado = 3,
        /// <summary>
        /// Pedido completado
        /// </summary>
        Recibido=4


    }
}
