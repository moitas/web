﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Shop.CORE
{
    /// <summary>
    /// Entidad de dominio de "Producto"
    /// </summary>
    public class OrderLine
    {
        /// <summary>
        /// Identificador del producto
        /// </summary>
        public int Id { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        /// 
         [Display(Name = "Descripcion")]
        public string Description { get; set; }

        /// <summary>
        /// Precio del producto actual
        /// </summary>
        /// 
        [Display(Name = "Precio(€)")]
        [Required(ErrorMessage = "Verifica el precio entre 0-999")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        public decimal Price { get; set; }

        /// <summary>
        /// Cantidad del prodcuto
        /// </summary>
        /// 
        [Display(Name = "Cantidad")]
        public int Quantity { get; set; }

        /// <summary>
        /// Total de la linea del pedido
        /// </summary>
        [Display(Name = "Subtotal")]
        public decimal SubTotal { get; set; }

        /// <summary>
        /// Pedido al que pertenece la linea
        /// </summary>
        public Order Order { get; set; }

        /// <summary>
        /// Identificador del pedido al que pertenece la linea
        /// </summary>
        [ForeignKey("Order")]
        public int OrderId { get; set; }
  
        /// <summary>
        /// Usuario que crea la linea
        /// </summary>
        public ApplicationUser User { get; set; }


        /// <summary>
        /// Identificador del usuario
        /// </summary>
        [ForeignKey("User")]
        public string User_Id { get; set; }


        /// <summary>
        /// Prodiucto Sin eliminacion en cascada
        /// </summary>
        ///
        [ForeignKey("Product")]
        public int ProductId { get; set; }

        /// <summary>
        /// Producto que trata la linea
        /// </summary>
        public Product Product { get; set; }


    }
}
