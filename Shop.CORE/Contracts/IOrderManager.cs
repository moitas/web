﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.CORE.Contracts
{
    public interface IOrderManager : IGenericManager<Order>
    {

        Order GetByIdAndLines(int id);
        Order GetByIdAndUserId(int id, string userId);

        IQueryable<Order> GetByUserId(string userId);

    }
}
