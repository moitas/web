﻿using System.Data.Entity;
using Shop.CORE;
using System.Data.Entity.Infrastructure;
using System;

namespace Shop.CORE.Contracts
{
    public interface IApplicationDbContext
    {

        DbEntityEntry Entry(object entity);
        
        DbEntityEntry<TEntity> Entry<TEntity>(TEntity entity) where TEntity : class;

        int SaveChanges();

        DbSet Set(Type entityType);

        DbSet<TEntity> Set<TEntity>() where TEntity : class;


        DbSet<Order> Orders { get; set; }

        //DbSet<OrderLine> OrderLines { get; set; }

        //DbSet<Cart> Carts { get; set; }

        //DbSet<CartItem> CartItems { get; set; }


        DbSet<Product> Products { get; set; }

        //DbSet<Address> Addresss { get; set; }

        DbSet<ExceptionLogger> ExceptionLoggers { get; set; }


    }
}