﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.CORE.Contracts
{
    public interface ICartItemManager : IGenericManager<CartItem>
    {
        //CartItem GetByUserId(string userId);
        IQueryable<CartItem> GetByUserId(string userId);

        void RemoveByUserId(string userId);

        CartItem GetByProductIdAndCartId(int cartId, int productId);

    }
}
