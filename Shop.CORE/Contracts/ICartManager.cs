﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Shop.CORE.Contracts
{
    public interface ICartManager : IGenericManager<Cart>
    {

        Cart GetByIdAndItems(int id);


        Cart GetByUserId(string userId);



        //IEnumerable<Cart> RemoveRange(IEnumerable<Cart> entities);

        //void RemoveAll<T>(this ICollection<T> collection, Predicate<T> predicate);

    }
}
