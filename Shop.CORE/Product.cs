﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;


namespace Shop.CORE
{

    public class Product

    {

        /// <summary>
        /// Identificador del producto
        /// </summary>
        /// 
        [Display(Name = "COD")]
        public int ProductId { get; set; }

        /// <summary>
        /// Descripcion del producto
        /// </summary>
        /// 
        [MaxLength(100), MinLength(3)]
        [Required(ErrorMessage = "Nada de anónimos. ¡Aquí todo tiene un nombre!")]
        [Display(Name = "Descripción")]
        public string Description { get; set; }

        /// <summary>
        /// Descripcion extendida del producto
        /// </summary>
        /// 
        [Required(ErrorMessage = "Falta el detalle")]
        [MaxLength(100)]
        [Display(Name = "Detalle")]        /// 
        public string DescriptionLarge { get; set; }


        /// <summary>
        /// Cantidad de producto en unidades
        /// </summary>
        /// 
        [Required(ErrorMessage = "Verifica el stock entre 0-999")]
        [Display(Name = "Stock")]
        [Range(0, 999)]
        public int Stock { get; set; }



        /// <summary>
        /// Precio del producto actual
        /// </summary>
        /// 
        [Display(Name = "Precio(€)")]
        //[DataType(DataType.Currency)]
        [Required(ErrorMessage = "Verifica el precio entre 0-999")]
        [DisplayFormat(DataFormatString = "{0:C0}", ApplyFormatInEditMode = true)]
        //[DisplayFormat(DataFormatString = "{0:C0}")]
        [Range(1, 999, ErrorMessage = "Entre de 1 9999")]
        public decimal Price { get; set; }


        /// <summary>
        /// Imagen  número 1
        /// </summary>
        /// 
        [Display(Name = "Imagen principal")]
        public byte[] ProductImage1 { get; set; }


        /// <summary>
        /// Imagen número 2
        /// </summary>
        /// 
        [Display(Name = "Imagen 2º")]
        public byte[] ProductImage2 { get; set; }

        /// <summary>
        /// Imagen numero 3
        /// </summary>
        /// 
        [Display(Name = "Imagen 3º")]
        public byte[] ProductImage3 { get; set; }



        //public object Name { get; set; }



        //public virtual List<Image> Images { get; set; }



        //public int CategoryId { get; set; }
        //public Category Category { get; set; }




    }



}
