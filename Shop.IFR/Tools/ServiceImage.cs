﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Shop.IFR.Tools
{
    public class ServiceImage
    {
        /// <summary>
        /// METODO QUE CONVIERTE UNA IMAGEN A BINARIO
        /// </summary>
        /// <param name="image"></param>
        /// <returns></returns>
        public static byte[] ConvertToBytes(HttpPostedFileBase image)
        {
            if (image == null)
            {
                return null;
            }
            else
            {
                byte[] imageBytes = null;
                BinaryReader reader = new BinaryReader(image.InputStream);
                imageBytes = reader.ReadBytes((int)image.ContentLength);
                return imageBytes;
            }
        }

    }
}
