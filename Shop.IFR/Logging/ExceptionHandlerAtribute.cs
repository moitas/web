﻿using Shop.CORE;
using Shop.CORE.Contracts;
using Shop.IFR.IoC;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;



namespace Shop.IFR.Logging
{

    public class ExceptionHandlerAttribute : FilterAttribute, IExceptionFilter
    {
        IExceptionLoggers exceptionLoggers = null;

        public ExceptionHandlerAttribute()
        {
            exceptionLoggers = (IExceptionLoggers)UnityConfig.Container.Resolve(typeof(IExceptionLoggers), "loggers");
        }

        public void OnException(ExceptionContext filterContext)
        {
            if (!filterContext.ExceptionHandled)
            {

                ExceptionLogger logger = new ExceptionLogger()


                {
                    ExceptionMessage = filterContext.Exception.Message,
                    ExceptionStackTrace = filterContext.Exception.StackTrace,
                    ControllerName = filterContext.RouteData.Values["controller"].ToString(),
                    LogTime = DateTime.Now
                };


                exceptionLoggers.Add(logger);
                exceptionLoggers.Context.SaveChanges();

                filterContext.ExceptionHandled = false;
            }
        }
    }


}



