using Shop.CORE.Contracts;
using System;
using Unity;
using Unity.AspNet.Mvc;
using Unity.Lifetime;
//using Microsoft.Practices.Unity;
//using Shop.CORE;

namespace Shop.IFR.IoC
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public static class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container =
          new Lazy<IUnityContainer>(() =>
          {
              var container = new UnityContainer();
              RegisterTypes(container);
              return container;
          });

        /// <summary>
        /// Configured Unity Container.
        /// </summary>
        public static IUnityContainer Container => container.Value;
        #endregion
        
        /// <summary>
        /// Registers the type mappings with the Unity container.
        /// </summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>
        /// There is no need to register concrete types such as controllers or
        /// API controllers (unless you want to change the defaults), as Unity
        /// allows resolving a concrete type even if it was not previously
        /// registered.
        /// </remarks>
        /// 
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below.
            // Make sure to add a Unity.Configuration to the using statements.
            // container.LoadConfiguration();


            // TODO: Register your type's mappings here.
            // container.RegisterType<IProductRepository, ProductRepository>();

            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IApplicationDbContext, Shop.CORE"), Type.GetType("Shop.DAL.ApplicationDbContext, Shop.DAL"), new PerRequestLifetimeManager());

            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ILineManager, Shop.CORE"), Type.GetType("Shop.Application.LineManager, Shop.Application"));
            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IOrderManager, Shop.CORE"), Type.GetType("Shop.Application.OrderManager, Shop.Application"));
            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IProductManager, Shop.CORE"), Type.GetType("Shop.Application.ProductManager, Shop.Application"));

            container.RegisterType(Type.GetType("Shop.CORE.Contracts.IExceptionLoggers, Shop.CORE"), Type.GetType("Shop.Application.ExceptionLoggers, Shop.Application"), "loggers");

            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ICartManager, Shop.CORE"), Type.GetType("Shop.Application.CartManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.ICartItemManager, Shop.CORE"), Type.GetType("Shop.Application.CartItemManager, Shop.Application"));
            //container.RegisterType(Type.GetType("Shop.CORE.Contracts.IAddressManager, Shop.CORE"), Type.GetType("Shop.Application.AddressManager, Shop.Application"));

        }
    }
}